<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$url = 'https://share.mailbox.org/ajax/share/0baf84f90c46e84ebe9c8c1c46e84ace8abc1c353555ae64/1/2/Y2FsOi8vMC80Mw';

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $url,
    CURLOPT_HEADER => 0
));
$result = curl_exec($curl);
curl_close($curl);

echo icsToJson($result);

function icsToJson($icsString)
{
    $icsEvents = explode("BEGIN:VEVENT", $icsString);
    $icsEvents = array_slice($icsEvents, 1);

    $jsonEvents = array();

    foreach ($icsEvents as $event) {
        $startDate = "";
        $endDate = "";
        $summary = "";
        $description = "";

        $eventLines = explode("\n", $event);

        foreach ($eventLines as $line) {
            if (strpos($line, "DTSTART") !== false) {
                $startDate = trim(explode(":", $line)[1]);
            } elseif (strpos($line, "DTEND") !== false) {
                $endDate = trim(explode(":", $line)[1]);
            } elseif (strpos($line, "SUMMARY") !== false) {
                $summary = trim(str_replace("SUMMARY:", "", $line));
            } elseif (strpos($line, "DESCRIPTION") !== false) {
                $description = trim(str_replace("DESCRIPTION:", "", $line));
            }
        }

        if (!empty($startDate) && !empty($summary)) {
            $jsonEvents[] = array(
              "title" => $summary,
              "start" => date("Y-m-d H:i:s", strtotime($startDate)),
              "end" => date("Y-m-d H:i:s", strtotime($endDate)),
              "description" => $description
            );
        }
    }

    return json_encode($jsonEvents);
}
