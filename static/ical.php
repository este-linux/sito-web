<?php
header('Content-Type: text/calendar; charset=UTF-8');

$url = 'https://share.mailbox.org/ajax/share/0baf84f90c46e84ebe9c8c1c46e84ace8abc1c353555ae64/1/2/Y2FsOi8vMC80Mw';

$curl = curl_init();
curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent: curl/7.68.0');
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $url,
    CURLOPT_HEADER => 0
));
$result = curl_exec($curl);
curl_close($curl);

echo $result;
