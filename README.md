# Repository del sito web di este.linux.it

## Come scaricare

```bash
git clone --recursive https://gitea.it/openit-este/sito-web.git
```

## Come aggiornare

```bash
git pull
git submodule update --init --recursive --force
```

## Come compilare

Per testare in tempo reale:

```bash
hugo server -D
```

## Organizzazione contenuti

Tutti i contenuti sono nella cartella `content` e hanno lo stesso nome dei menu.
