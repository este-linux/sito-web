# Repository del sito web di este.linux.it

## Come compilare
```bash
git clone https://gitea.it/openit-este/sito-web.git
cd sito-web
git submodule add https://github.com/luizdepra/hugo-coder.git themes/hugo-coder
```

Per testare in tempo reale:
```bash
hugo server -D
```

## Organizzazione contenuti

Tutti i contenuti sono nella cartella `content` e hanno lo stesso nome dei menu.