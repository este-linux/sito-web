---
title: Rassegna stampa
slug: rassegna-stampa
date: 2024-11-28T21:56:00+02:00
include_donations: true
IgnoreTableOfContents: true
fullcover: true
images:
  - pinguini-rassegna-stampa.webp
---

Puoi contattarci direttamente su **estelinux(@)mailbox.org**.

Questi sono articoli che parlano di noi:

---
**Servizi Liberi su AlternativaLinux** (11/11/2024)

https://www.youtube.com/watch?v=IxzO52I5glU ('34)

---

**Pavia ILS** (06/11/2024)

https://pavia.ils.org/servizi-liberi/

---
**Rivista Bricks: Software Libero e Linux a scuola** (29/09/2024)

http://www.rivistabricks.it/wp-content/uploads/2024/09/BRICKS_4_2024_24_Lovato.pdf

---
**LeAlternative.net** (01/07/2024)

https://www.lealternative.net/2024/07/01/un-servizio-di-applicazioni-gratuite-e-libere-per-chiunque/

---
**Librezilla** (06/02/2024)

https://librezilla.it/
