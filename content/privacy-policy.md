---
title: Privacy Policy
description: Questa pagina descrive la privacy policy di questo sito web.
date: 2022-12-19T21:56:00+02:00

---
### Dati Personali trattati per le seguenti finalità e utilizzando i seguenti servizi:
#### 🗄️ Hosting

**🇪🇺 🇮🇹 Associazione Italian Linux Society**

Dati Personali:
- indirizzo IP secondo quanto specificato dalla privacy policy del servizio

#### 🗄️ Statistiche

**🇪🇺 🇮🇹 Associazione Italian Linux Society**

Dati Personali:
- indirizzo IP temporaneamente, poi convertito in hash e rimosso

### Ulteriori informazioni sul trattamento dei Dati Personali

**Descrizione del servizio**

Questo sito web ha lo scopo di informare e non ha scopo di lucro. Non raccoglie dati personali e sensibili degli utenti.

**Periodo di conservazione dei Dati Personali**

I Dati Personali raccolti vengono conservati dall'inizio della navigazione. Tali Dati Personali vengono registrati per adempiere agli obblighi legali che impongano la conservazione dei Dati Personali, in ottemperanza a quanto previsto dalla legge.

### Informazioni di contatto
**Titolare del Trattamento dei Dati**

🧑 Italian Linux Society

🏠 via Aldo Moro, 223, 92026 Favara (AG)

✉️ Email: presidente (at) linux.it
