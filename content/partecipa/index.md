---
title: Partecipa
slug: Partecipa
date: 2023-03-19T12:00:00+02:00
include_donations: true
images:
  - raspberry-pi-g5d1a1318c_1920.webp
---


## Attività

Le attività in programma sono:
 - incontri fisici per:
   - conoscerci :-)
   - installare **GNU/Linux** a chi lo volesse provare sul proprio pc o portatile
   - trovare e provare alternative libere a app, programmi o servizi di uso comune per ridurre la **profilazione** e riprenderci la propria **privacy**
   - recuperare vecchi pc, portatili o anche solo alcuni componenti per rimettere in piedi un nuovo pc, ed eventualmente donarlo a chi ne ha bisogno (ovviamente con GNU/Linux installato)
   - condividere esperienze personali
   - mappare le zone limitrofe con Openstreetmap, la mappa mondiale libera collaborativa (vedi il [progetto Mappami](/progetti/mappami/))
   - creare eventi di divulgazione (presentazioni, eventi nelle scuole, Linux Day, ecc)
 - chat nel [gruppo Telegram](https://t.me/openit_este) per:
   - conoscerci da remoto
   - condividere notizie, servizi liberi o programmi open-source
   - coordinarci per gli incontri fisici

La serata ogni tanto si conclude con un aperitivo :-)

![Linux in Veneto](linux-veneto.webp)

## Quando

Gli incontri sono visibili nel calendario:

{{< calendar >}}

Attualmente sono tenuti nel **Patronato Ss. Redentore di Este**.

Si entra dalla via principale di Principe Umberto, poi nel parcheggio grande e dal parcheggio si arriva a una galleria. L'entrata poi è a metà galleria circa, dal portone rosso:

[📍 Entrata](https://www.qwant.com/maps/place/osm:way:158649144@Patronato_Santissimo_Redentore#map=17.50/45.2248810/11.6571771)
