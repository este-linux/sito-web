---
title: Partecipa
slug: Partecipa
date: 2023-03-19T12:00:00+02:00
include_donations: true
images:
  - raspberry-pi-g5d1a1318c_1920.webp
---

## Attività

Le attività in programma sono:
 - incontri fisici per:
   - conoscerci :-)
   - installare **GNU/Linux** a chi lo volesse provare sul proprio pc o portatile
   - trovare e provare alternative libere a app, programmi o servizi di uso comune per ridurre la **profilazione** e riprenderci la propria **privacy**
   - recuperare vecchi pc, portatili o anche solo alcuni componenti per rimettere in piedi un nuovo pc, ed eventualmente donarlo a chi ne ha bisogno (ovviamente con GNU/Linux installato)
   - condividere esperienze personali
   - mappare le zone limitrofe con Openstreetmap, la mappa mondiale libera collaborativa (vedi il [progetto Mappami](/progetti/mappami/))
   - creare eventi di divulgazione (presentazioni, eventi nelle scuole, Linux Day, ecc)
 - chat nel [gruppo Telegram](https://t.me/openit_este) per:
   - conoscerci da remoto
   - condividere notizie, servizi liberi o programmi open-source
   - coordinarci per gli incontri fisici

La serata ogni tanto si conclude con un aperitivo :-)

![Linux in Veneto](linux-veneto.webp)

## Quando

Gli incontri sono visibili nel [calendario degli eventi](/eventi).
Attualmente sono tenuti nel **Patronato Ss. Redentore di Este**.

Si entra dalla via principale di Principe Umberto, poi nel parcheggio grande e dal parcheggio si arriva a una galleria. L'entrata poi è a metà galleria circa, dal portone rosso:

[📍 Entrata](https://www.qwant.com/maps/place/osm:way:158649144@Patronato_Santissimo_Redentore#map=17.50/45.2248810/11.6571771)

## Come rimanere in contatto

### Telegram
Devi creare un account su **Telegram** ed entrare nel gruppo cercando **ILS Este - Gruppo** o cliccando qui: https://t.me/openit_este

- [Scarica Telegram dal Play Store](https://play.google.com/store/apps/details?id=org.telegram.messenger)
- [Scarica Telegram da F-Droid](https://f-droid.org/it/packages/org.telegram.messenger/)
- [Scarica Telegram da App Store](https://apps.apple.com/it/app/telegram-messenger/id686449807)


### Matrix
E' un canale alternativo e decentralizzato ma sincronizzato con quello di Telegram. Devi creare un account su **Matrix** ed entrare nel gruppo dopo aver scaricato un client per Matrix, noi consigliamo **Element**. Dopodiché dovrai unirti alla stanza da qui: https://matrix.to/#/!FQaIJQacPkLZHJaMfQ:matrix.org

- [Scarica Element dal Play Store](https://play.google.com/store/apps/details?id=im.vector.app)
- [Scarica Element da F-Droid](https://f-droid.org/en/packages/im.vector.app/)
- [Scarica Element da App Store](https://apps.apple.com/it/app/element/id1083446067)

### Email
Per chi vuole rimanere aggiornato solo su notizie importanti o eventi, consiglio di iscriversi alla mailing-list (massimo 2-3 al mese più o meno):
 - https://lists.linux.it/listinfo/este