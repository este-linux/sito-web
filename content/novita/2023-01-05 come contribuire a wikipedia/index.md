---
title: Come funziona (e come contribuire a) Wikipedia
description: Breve spiegazione di come funziona Wikipedia e come iniziare a contribuire attivamente
date: 2023-01-05T15:37:00+02:00
authors:
    - loviuz
slug: come-contribuire-a-wikipedia
images:
    - wikipedia-logo.webp
layout: news
tags:
    - wikipedia
    - wikimedia
    - cultura
---

## Cos'è

Wikipedia è un'enciclopedia online libera, creata e gestita da una comunità di volontari. Ogni articolo è scritto e verificato da un gruppo di collaboratori e può essere modificato da chiunque, sia che sia un esperto o un appassionato. Wikipedia si basa sul principio del wikismo, ovvero l'idea che la conoscenza deve essere condivisa e accessibile a tutti, e include una vasta gamma di argomenti, dalla storia alla scienza, dalla cultura all'arte.

## Quali sono i rischi?

Uno dei possibili svantaggi di Wikipedia è che gli articoli possono essere modificati da chiunque, il che significa che a volte le informazioni possono essere errate o incomplete. Tuttavia, gli editori di Wikipedia fanno del loro meglio per verificare e correggere gli errori, e gli articoli sono spesso sottoposti a revisione prima di essere pubblicati. Inoltre, Wikipedia include un sistema di referenze che permette di verificare le fonti delle informazioni e un sistema di discussione per cui è possibile interagire con i vari utenti e editori per decidere come apportare delle modifiche.

Immaginate le questioni sul Covid, i vaccini e sull'attuale invasione russa in Ucraina: se ne sono dette di tutti i colori e infatti anche nelle relative pagine Wikipedia ci sono parecchie discussioni su queste pagine o altre pagine collegate (il battaglione Azov, ecc). Devo dire che i moderatori sono veramente in gamba, almeno quelli che ho incontrato io, perché non sono persone messe lì da qualcuno ma sono utenti con anni di esperienza e contributi che sanno mediare ed essere obiettivi. Non c'è una fonte su una notizia delicata? Non si scrive nulla. L'enciclopedia è fatta per scrivere i fatti avvenuti, non teorie o possibili evoluzioni.

## Proviamola!

Anche se è possibile contribuire in minima parte senza registrarsi, è quindi necessario registrarsi così da contribuire usando il proprio nickname e usare tutte le funzionalità al completo.

![Benvenuto](benvenuto.webp)

Dopo aver cliccato in alto a destra su **Discussioni** è possibile vedere un'introduzione della pagina di Wikipedia, dove ci sono un messaggio di benvenuto e altre sezioni che servono da guida introduttiva; si sono anche anche un tour guidato, raccomandazioni, come gestire i problemi di copyright sul materiale caricato, progetti tematici e glossario.

Se invece clicchiamo su **Contributi** in alto a destra vediamo uno storico delle nostro modifiche.

Rimanendo con il mouse sopra a **Contributi** apparirà un sotto-menu dove troviamo **Traduci**: da questa pagina è possibile scegliere uno degli articoli da tradurre tra i suggerimenti:

![Suggerimenti traduzioni Wikipedia](suggerimenti-traduzioni-wikipedia.webp)

Se clicchiamo su uno di questi suggerimenti si aprirà uno strumento per aiutare nella traduzione. Rispetto a tempo fa, dove si doveva copiare tutto manualmente, ora è disponibile questo strumento ausiliario che traduce blocco per blocco la pagina, mantenendo i link alle fonti, allo stile (grassetto, corsivo, ecc) ed è quindi tutto molto più rapido! Ovviamente il testo va controllato, infatti durante la traduzione appariranno sulla destra degli avvisi di quanto il testo tradotto automaticamente sia diverso da quello digitato, per stimolare la contro-verifica prima di pubblicare la pagina tradotta.

![Traduzione automatica](traduzione-automatica.webp)

## Pubblicazione

Al termine della traduzione si preme su **Pubblica** e la pagina sarà pubblicata immediatamente su Wikipedia!

![Contribuire su Wikipedia è veloce](wikipedia-contribuire-veloce.webp)

Ma è davvero così veloce e semplice contribuire su Wikipedia? Sì! 😯

E altrettanto veloce è anche la revisione dai moderatori. Vi faccio un esempio reale: ho tradotto la pagina della [Basilica di Santa Maria delle Grazie](https://en.wikipedia.org/wiki/Santa_Maria_delle_Grazie,_Este) che era solo in inglese. L'indirizzo era:

```
https://en.wikipedia.org/wiki/Santa_Maria_delle_Grazie,_Este
```

La traduzione italiana avviene nel sito italiano che non è `en.wikipedia.org` ma `it.wikipedia.org` e ha delle regole e forme un po' diverse, giusto per uniformare i contenuti secondo gli stessi standard qualitativi. Per evitare ambiguità, Wikipedia ha introdotto delle [regole per evitare pagine con nomi simili o ambigui](https://it.wikipedia.org/wiki/Aiuto:Disambiguazione). Io avevo creato la pagina italiana con il titolo `Santa_Maria_delle_Grazie,_Este`, duplicando banalmente la versione inglese, invece di scrivere `Santa_Maria_delle_Grazie_(Este)` poiché c'è almeno un'altra pagina che si chiama **Santa Maria delle Grazie**. L'avviso che la pagina è stata cancellata e spostata è arrivato dopo circa un paio di minuti, pazzesco! E l'indirizzo è stato ulteriormente migliorato in `Basilica_di_Santa_Maria_delle_Grazie_(Este)`. E' giusto così, ma io avevo semplicemente duplicato la pagina inglese sperando che la forma andasse bene.

Niente paura, i moderatori e editori sono lì apposta per supportare gli altri utenti.

![Regole Wikipedia](regole-wikipedia.webp)

## Altri suggerimenti

Cliccando in alto a destra sul proprio nickname vi troverete in una pagina con alcuni collegamenti interessanti. Non ho lo screenshot iniziale, ma prima di procedere vi chiederà quali sono i vostri interessi (arte, musica, politica, ecc), così che Wikipedia possa suggerirti di contribuire nei settori di tuo interesse. Ti chiederà poi con quale livello di difficoltà vuoi contribuire: io ho scelto **facile** per iniziare. Subito dopo appariranno una serie di pagine di livello facile dove contribuire. In genere sono pagine già complete con imprecisioni o errori grammaticali segnalati da altri utenti, e viene indicato anche quanto tempo può richiedere il contributo, in questo caso 5-10 minuti:  

![Suggerimenti](suggerimenti.webp)

Cliccando su **Facile** è sempre possibile ambiare il livello di difficoltà dei suggerimenti che si intende ricevere:

![Difficoltà suggerimenti](difficolta.webp)


Poco più a destra troviamo il riquadro **Il tuo impatto**: è un riquadro che mostra quante volte sono state visitate le pagine dalla nostra ultima modifica, negli ultimi 60 giorni. Questo dovrebbe invogliare a contribuire più spesso vedendo che le pagine vengono lette dopo aver contributo. 😌


## Tutor

Infine scopro che ci viene assegnato un **tutor**, ossia un utente con più esperienza che può rispondere direttamente a dubbi e domande! Ovviamente si può scrivere a qualsiasi utente, ma il fatto che sia stato previsto un utente lì comodo a cui scrivere è secondo me una gran bella idea.

![Tutor](tutor.webp)
