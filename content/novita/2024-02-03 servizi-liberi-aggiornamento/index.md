---
title: ServiziLiberi.it ora con un aggregatore di notizie (RSS) e traduttore
description: Riscopri un modo diverso di informarti e traduci liberamente i tuoi testi
date: 2024-02-03T19:07:00+02:00
authors:
    - loviuz
slug: servizi-liberi-rss-traduttore
images:
    - servizi-liberi-nuovi.webp
layout: news
fullcover: false
tags:
    - ils
    - serviziliberi
    - rss
    - informazione
    - traduzioni
IgnoreTableOfContents: true
---

Dopo il primo lancio di [ServiziLiberi](/novita/servizi-liberi) con gli **strumenti per i PDF** e un'applicazione per le **ricette di cucina**, oggi lanciamo due nuovi servizi:
 - **aggregatore di notizie (RSS):** il software libero usato si chiama [FreshRSS](https://www.freshrss.org/). Il formato [RSS](https://it.wikipedia.org/wiki/RSS) ultimamente va poco di moda, ma su molti siti è ancora utilizzato e molte persone preferiscono informarsi **scegliendo le fonti delle notizie** per consultarle in ordine cronologico piuttosto che farsi suggerire che cosa leggere, cosa sapere e cosa non sapere tramite algoritmi che utilizzano i propri dati, le proprie abitudini e preferenze. E' possibile configurare le proprie fonti di notizie tramite i feed RSS dei siti che si vogliono seguire, e automaticamente si avrà una pagina sempre aggiornata con le notizie in ordine cronologico, senza profilazione, senza cookie, senza pubblicità mirata e senza alcun suggerimento o distrazione.
 - **traduttore:** dove andate a tradurre testi o frasi solitamente? Google Translate?? NO, dovete utilizzare ServiziLiberi grazie al software libero [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate) 🤓! I testi e le frasi che tradurrete qui non vengono salvati, e l'accesso al sito stesso non vi profila. E' un traduttore che fa un'unica cosa: tradurre, niente di più. Utilizza dei **modelli linguistici pre-addestrati**, per cui genera le traduzioni in modo dinamico e in tempo reale. I modelli linguistici scaricati sul nostro server pesano circa 10 GB.

## Una curiosità sull'aggregatore di notizie (RSS)
L'idea dell'aggregatore di notizie è nato dopo il nostro ultimo incontro. Il linuxiano **Michele** usa da almeno 10 anni i feed RSS per informarsi. Su un suo server, utilizza [Tiny Tiny RSS](https://tt-rss.org/), ed era stata la prima scelta per ServiziLiberi, ma il problema è che non è pensato, purtroppo, per essere multi-utente, per cui le fonti di notizie inserite sarebbero state le stesse per chiunque lo avrebbe consultato, mentre con FreshRSS è possibile crearsi un profilo personale e scegliere le proprie fonti da seguire.

I feed RSS sono molto meno usati rispetto ad anni fa. Le grandi piattaforme, ma anche molti siti web di informazione, preferiscono far accedere gli utenti proprio al loro sito web per poterli profilare, facendo loro accettare i cookie in modo a volte poco chiaro o ingannevole. Non vogliamo fare nomi, ma accedendo a un noto sito web di informazione italiana, se ci addentriamo nella scelta di quali cookie accettare, notiamo da quante aziende verremmo profilati accettando di continuare:

![profilazione](profilazione.webp)

Il tasto **Rifiuta e abbonati** che, invece, sembra andare di moda, è ancora [al vaglio del Garante della Privacy](https://ntplusdiritto.ilsole24ore.com/art/paywall-monetizzazione-dati-e-rischio-privacy-come-lusso-pochi-AFVxyXB) poiché potrebbe non essere legittimo.

Usare un lettore di feed RSS ha quindi 2 scopi importanti:
 - riprendersi la **libertà di scelta** di come informarsi
 - informarsi senza dare in cambio, ancora una volta, **i nostri dati personali**, a volte richiesti in modo ingannevole

Informarsi utilizzando gli aggregatori di notizie tramite feed RSS non è semplice per chi non ha mai provato, ma da un [breve sondaggio nel nostro gruppo Telegram](https://t.me/openit_este/3211) emerge che pochissime persone non lo conoscono come strumento, per cui abbiamo pensato di metterlo a disposizione ed eventualmente fare un approfondimento più avanti sul suo utilizzo.

![meme rss](meme-rss.webp)


{{< call-to-action title="Accedi a ServiziLiberi.it ➡️" url="https://serviziliberi.it" >}}

All'interno trovi anche come contribuire con una donazione poiché preferiamo sostenere questi servizi in euro piuttosto che raccogliendo i tuoi dati e con metodi poco trasparenti.

## 🙄 Non è chiaro?
Se non hai capito nulla ma ti sembra utile, interessante, o allarmante, non ti preoccupare. Abbiamo un gruppo Telegram chiamato **ILS Este - Gruppo** (ti puoi unire da https://t.me/openit_este se hai Telegram), e **ci troviamo periodicamente a Este**, trovi tutte le informazioni nella pagina [Partecipa](/partecipa/).