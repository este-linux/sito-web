---
title: "Computer, telefoni e internet: cosa abbiamo nelle nostre mani?"
description: Primo incontro di un percorso alla scoperta di questi strumenti e delle alternative libere
date: 2024-04-07T16:57:00+02:00
authors:
    - loviuz
slug: computer-telefoni-internet
images:
    - computer-telefoni-internet.webp
layout: news
fullcover: true
tags:
    - ils
    - computer
    - telefono
    - internet
    - libertà
IgnoreTableOfContents: true
---

Sabato 27 aprile saremo ospiti del [Comitato Meggiaro-Este Nuova](https://www.facebook.com/profile.php?id=100090100116787), un comitato cittadino di quartiere che organizza eventi e attività.

Saremo presenti **dalle 14:30 alle 18:30 con il nostro laboratorio su Linux e software libero** come di consueto, ma la presentazione e il confronto saranno dalle **16:00 alle 17:00** circa. L'indirizzo è **Via Abramo Lincoln, 23/25, Este (PD)**, presso la sala polivalente, e l'ingresso è **gratuito**.

Da qualche mese eravamo in contatto alla ricerca di una sede dove organizzare i nostri incontri e li ringraziamo tantissimo per questa opportunità!

L'incontro di sabato sarà il primo, nel tentativo di portare la **filosofia del software libero** a persone di ogni età e grado di conoscenza informatica.

## Perché?

Oggi tutti utilizziamo un telefono o un computer per navigare in rete. Anche quando non facciamo nulla attivamente accadono cose: il telefono registra la nostra posizione geografica e la invia ad alcuni servizi, quando apriamo un sito ci viene chiesto di accettare qualcosa che sembra innocuo, quando installiamo un'app o un programma (detto anche software) non sappiamo esattamente cosa fa, ma ci serve. Che cosa accade realmente quando proseguiamo accettando tutto senza porci domande?

## Cos'è il software libero?

C'è una soluzione a tutto questo ed è il software libero: una serie di programmi e progetti portati avanti da volontari (e non) di tutto il mondo. Tecnologie che non hanno scopo di lucro e che fanno esclusivamente ciò per cui sono state progettate, non per carpire i nostri dati e convincerci con l'inganno che l'uso è gratuito quando in cambio veniamo profilati e seguiti in rete per venderci qualche prodotto o per proporci che cosa leggere credendo di avere la libertà di scelta.

Come facciamo a fidarci che non sono "cattive"? Sono libere e aperte, nel senso che le possiamo "aprire" e guardarci dentro, e addirittura le possiamo modificare e ridare indietro a questa enorme comunità, migliorandole per tutte le persone che le stanno usando.

Non è fantastico? 🥹

## Come partecipare

Per una migliore organizzazione ti chiediamo di iscriverti qui:

{{< call-to-action title="✔️ Voglio partecipare" url="https://mobilizon.it/events/882c51fe-135a-4429-9258-85da0943f6ae" >}}

(Naturalmente, già dall'iscrizione, stiamo usando un'applicazione libera che non traccia e non chiede i tuoi dati personali. Sei già sulla buona strada! 😃)

E' consigliato **portare almeno il tuo telefono**, che immaginiamo tu abbia sempre rigorosamente con te.
