---
title: Passa a Linux!
description: Liberati di Windows o Mac, e torna a usare un sistema operativo libero.
date: 2023-05-19T23:12:00+02:00
IgnoreTableOfContents: true
authors:
    - loviuz
slug: passa-a-linux
images:
    - passa-a-linux.webp
layout: news
fullcover: true
tags:
    - linux
    - tecnologie
    - softwarelibero
    - alternative
---

## Perché Linux

Quando si utilizzano strumenti digitali connessi a internet, come un computer o un telefono, ci si espone a diversi rischi e si è profilatɜ costantemente. Le alternative valide e funzionanti ci sono, ma la nostra idea è che si deve partire dal sistema operativo, cioè dal programma principale su cui gira tutto, per questo proponiamo di passare subito a Linux!

1. Linux è software libero, il che significa che chiunque può accedere al codice sorgente e modificarlo liberamente, mentre Windows è un sistema operativo proprietario e il cui codice sorgente non è accessibile al pubblico.

2. Linux garantisce una maggiore protezione della privacy rispetto a Windows, in quanto non invia informazioni delle persone a terze parti senza il loro consenso esplicito.

3. Linux offre una maggiore flessibilità e personalizzazione rispetto a Windows, il che significa che glɜ utenti possono personalizzare il sistema operativo in base alle proprie esigenze.

4. Linux è generalmente più stabile e affidabile rispetto a Windows, in quanto è stato progettato per funzionare su una vasta gamma di dispositivi con specifiche diverse.

5. Linux promuove la democrazia digitale offrendo una piattaforma aperta e collaborativa, su cui le persone possono lavorare insieme per migliorarlo.

In sintesi, Linux è migliore di Windows perché offre maggiore libertà, flessibilità, sicurezza, stabilità e promuove la democrazia digitale.


## Quali sono i rischi?

Al contrario di Linux, l'uso di tecnologie chiuse può comportare diversi rischi per glɜ utenti di internet. Uno dei principali rischi è l'invasione della privacy, poiché le tecnologie chiuse possono essere sviluppate e utilizzate per fini illeciti, come la raccolta e la vendita di dati personali senza il suo consenso.

In particolare, il GDPR (General Data Protection Regulation), l'ultima legislazione europea sulla protezione dei dati personali, vieta la raccolta e l'elaborazione dei dati personali senza il consenso della persona esplicito e specifico. Tuttavia, molte aziende utilizzano ancora tecnologie chiuse per raccogliere dati personali senza il consenso dell'interessatɜ, mettendo a rischio i loro dati personali.

Inoltre, l'uso di tecnologie chiuse può comportare **rischi per la democrazia**, poiché le aziende che gestiscono i nostri dati personali possono utilizzarli per altri scopi che non riguardano la protezione della privacy. In particolare, gli Stati Uniti, in cui molte delle principali aziende tecnologiche hanno sede, hanno leggi meno restrittive sulla raccolta e l'utilizzo dei dati personali, il che significa che questi potrebbero essere utilizzati per altri scopi, come la **profilazione politica** o la manipolazione dei risultati delle elezioni.

Questo rappresenta un **grave rischio per la democrazia**, poiché la manipolazione dell'opinione pubblica attraverso i dati personali deglɜ utenti potrebbe influenzare l'esito delle elezioni e minare la fiducia nel processo democratico. Inoltre, l'uso di tecnologie chiuse potrebbe aumentare il **divario tecnologico** tra i paesi sviluppati e quelli in via di sviluppo, poiché i paesi più ricchi hanno più accesso alle tecnologie avanzate, mentre i paesi meno sviluppati sono più vulnerabili agli attacchi informatici e alla raccolta di dati non autorizzata.


## Che cosa facciamo?

Come molti altri gruppi Linux in Italia ([lugmap.linux.it](https://lugmap.linux.it)) e nel mondo, ci mettiamo a disposizione di tuttɜ per aiutarvi a installare un sistema operativo libero nel tuo computer, ma anche per aiutarti a usare altri strumenti o servizi liberi.


## Contattaci

Se vuoi sapere dove trovarci e quando, puoi [contattarci da qui](/partecipa/#come-rimanere-in-contatto)!
