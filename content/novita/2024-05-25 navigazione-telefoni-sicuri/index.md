---
title: "Come navigare in libertà e sicurezza"
description: "Scopriamo insieme, con un mini-corso pratico, come navigare su internet in libertà e sicurezza"
date: 2024-05-11T18:46:00+02:00
authors:
    - loviuz
slug: navigazione-libera-sicura
images:
    - navigare-in-liberta-sicurezza.webp
layout: news
fullcover: true
tags:
    - corso
    - computer
    - telefono
    - internet
    - privacy
    - profilazione
IgnoreTableOfContents: true
---

Sabato 25 maggio proseguiremo i nostri incontri con il [Comitato Meggiaro-Este Nuova](https://www.facebook.com/profile.php?id=100090100116787), un comitato cittadino di quartiere che organizza eventi e attività a Este (PD).


{{< quick_summary >}}
- l'incontro è **sabato 25 maggio** in **Via Abramo Lincoln, 23/25**, Este (PD), **dalle 16:00 alle 17:00**, ma ci trovi dalle 14:30 alle 18:30
- l'incontro è **solo in presenza** ed è **gratuito**
- devi portare almeno il telefono e/o il computer portatile. Carica la batteria prima di partire :-)
- imparerai a **installare app o programmi alternativi e liberi** per **navigare senza pubblicità personalizzata e i traccianti** che ti seguono in rete
- conferma la tua presenza in 10 secondi da qui: [🗓️ Conferma](https://mobilizon.it/events/09dfd50d-782d-4e8b-9ff3-6c4895287748)
{{< /quick_summary >}}



Saremo presenti **dalle 14:30 alle 18:30 con il nostro laboratorio su Linux e software libero** come di consueto, ma la presentazione e la parte pratica saranno **dalle 16:00 alle 17:00**. L'indirizzo è **Via Abramo Lincoln, 23/25, Este (PD)**, presso la **sala polivalente**, e l'ingresso è **gratuito**.

L'incontro di sabato sarà il secondo, nel tentativo di portare la **filosofia del software libero** a persone di ogni età e grado di conoscenza informatica.

## Perché?

La navigazione con il motore di ricerca più usato al mondo, Google, centralizza tutti i nostri dati su un'azienda che ricava, a livello mondiale, [più di 200 miliardi di dollari l'anno](https://www.analisicorporate.net/alphabet-google-risultati-trimestrali/). L'**economia dei dati** permette, a grosse aziende tecnologiche come Google e poche altre, di fornire buona parte di servizi e prodotti in modo gratuito: Gmail, YouTube, Google Photo, Google Maps, Whatsapp, Facebook, Office 365. Questi strumenti vengono forniti in cambio dei nostri dati personali per:
 - proporci continuamente e ovunque pubblicità personalizzata
 - creare nuovi contenuti di ogni tipo sfruttando l'IA (Intelligenza Artificiale), grazie a ciò che scriviamo in rete e dai nostri dati personali
 - proporci cosa leggere (algoritmi di suggerimento contenuti) e quando (notifiche)
 - sono centralizzati in un'unica azienda che può scegliere cosa non possiamo leggere, decidendo per gli utenti di tutto il mondo quali contenuti non mostrare


## Cosa faremo

### 1. Parte teorica

- Cos'è la normativa europea sulla privacy (**GDPR**) e perché è importante
- Cosa sono i famosi **cookie**, come funzionano e come bloccarli
- Come funzionano il **tracciamento** e la **profilazione** sul web.

### 2. Parte pratica

- Come sostituire il programma o l'app predefinita per navigare, sostituendola con una libera e più rispettosa della privacy: **Firefox**
- Aggiungere l'estensione che blocca la maggior parte dei traccianti e delle pubblicità personalizzate: **uBlock Origin**, con qualche modifica in più
- Quali motori di ricerca usare in sostituzione di Google, Bing o altri principali che spesso vengono pre-configurati nel telefono o nel programma di navigazione
- Vedremo come funzionano i **permessi delle app**, in modo da poterli abilitare in modo consapevole

## Come partecipare

Per una migliore organizzazione ti chiediamo di iscriverti qui:

{{< call-to-action title="✔️ Voglio partecipare" url="https://mobilizon.it/events/09dfd50d-782d-4e8b-9ff3-6c4895287748" >}}

(Naturalmente, già dall'iscrizione, stiamo usando un'applicazione libera che non traccia e non chiede i tuoi dati personali. Sei già sulla buona strada! 😃)

E' consigliato **portare almeno il tuo telefono e/o il tuo computer portatile**, che immaginiamo tu abbia sempre rigorosamente con te.

🪫 Carica la batteria prima di arrivare 🙂
