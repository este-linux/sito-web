---
title: Le attività di maggio
description: Partecipa con noi ai principali eventi di maggio!
date: 2023-04-28T23:26:00+02:00
IgnoreTableOfContents: true
authors:
    - loviuz
slug: attivita-maggio-2023
fullcover: true
images:
    - openit-este-maggio.webp
layout: news
tags:
    - eventi
    - tecnologie
    - hacktivismo
    - softwarelibero
    - mergeit
---

# Maggio ricca di eventi!

Questo maggio ci sono almeno 2 appuntamenti importanti.

## 📅 Reclaim The Tech - Bologna

*5-6-7 maggio*

Un festival, una fucina di scambi e riflessioni, un percorso da costruire insieme per riprenderci la tecnologia e rimetterla al servizio di persone e comunità.

Se vuoi venire con noi **sabato 6 maggio** puoi [contattarci](/partecipa/#come-rimanere-in-contatto)!

https://reclaimthetech.it/


## 📅 Merge-It - Verona

*12-13-14 maggio*

In Italia esistono tante realtà che si occupano di **libertà digitali**, sotto molti aspetti ed in molti modi: associazioni, gruppi informali, aziende, professionisti e pubbliche amministrazioni.
Lo scopo di MERGE-it è riunirle insieme, per conoscersi e farsi conoscere, discutere e dibattere, confrontarsi e misurarsi.

Se vuoi venire con noi **sabato 13 e domenica 14 maggio** puoi [contattarci](/partecipa/#come-rimanere-in-contatto)!

https://merge-it.net/


## 📅 Altri incontri online

Stiamo organizzando degli incontri online e in loco, vi aggiorneremo quanto prima. Tra le proposte abbiamo:
 - presentazione remota di Mastodon: cos'è, come funziona, perché usare un sociale decentralizzato
 - Minetest: videogiochi liberi e come programmarli 

Se vuoi rimanere aggiornato puoi [contattarci](/partecipa/#come-rimanere-in-contatto)!
