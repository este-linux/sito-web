---
title: Attività gennaio-febbraio 2024
description: Resoconto 2023 e prossimi sviluppi
date: 2024-02-17T22:49:00+02:00
authors:
    - loviuz
slug: attivita-gennaio-febbraio-2024
images:
    - attivita-inizio-2024.webp
layout: news
fullcover: false
tags:
    - ils
    - attivita
IgnoreTableOfContents: false
---

## ⏪ 2023: riassuntone stagione precedente

{{< img src="2023.webp" alt="Anno 2023" source="https://pixabay.com/it/illustrations/anno-anno-nuovo-domanda-labirinto-7685248/" >}}


🏁 Il 2023 è stato l'anno della nascita ufficiale del nostro gruppo. Partiti dal [canale Telegram](https://t.me/openit_este) chiamato **OpenIT Este** siamo diventati **sezione locale di ILS** ([Italian Linux Society](https://ils.org)) circa a giugno, quindi una sezione distaccata dell'associazione italiana che promuove Linux e il software libero. Abbiamo tenuto un po' di incontri in presenza per sperimentare e renderci disponibili per insegnare a usare Linux e strumenti digitali liberi.

🎤 A luglio e agosto abbiamo pubblicato [2 podcast](/tags/podcast/).

🎮 A settembre abbiamo organizzato [Giga Arena](/novita/torneo-videogiochi-liberi/), un torneo di videogiochi liberi per attrarre persone prendendoli per la gola con i videogiochi 😌

🐧 A ottobre si è tenuto il [Linux Day](/novita/linux-day-2023/), il mattino presso l'IIS Euganeo con gli studenti e il pomeriggio aperto a tutte le persone.

🧑‍🏫 A novembre abbiamo insegnato a bambini e bambine a **programmare giocando**, organizzando quindi un [CoderDojo](/novita/coderdojo-novembre-2023/) grazie all'aiuto di **Cristiano Marchioro** sotto il nome dell'associazione culturale **Sculdascia Lab**, ufficialmente chiusa ormai da un po' di anni ma viva di nome e di fatto almeno per il CoderDojo.

A dicembre abbiamo preso contatti con un istituto tecnico di Monselice per proporre il passaggio a Linux dei vari pc di laboratorio. Ci sono probabilmente tempi un po' più lunghi del previsto, se ne riparlerà verso primavera.

## 🚀 2024: la nostra rotta

Dopo 6 mesi di vita e di almeno un'attività al mese, ora si può provare a fare un cambio di passo.

{{< img src="2024.webp" alt="Anno 2024" source="https://pixabay.com/it/vectors/uomo-scogliera-saltando-disegno-8442149/" >}}


Siamo un gruppo Linux, ma ciò a cui teniamo è un uso consapevole della tecnologia, e per farlo proponiamo di usare il più possibile **strumenti digitali liberi**. Parliamo di "strumenti" in modo generico, anche se in gran parte parliamo di **software** (programmi). Per chi ancora non lo sapesse, ci sono software chiusi e aperti. Nel software chiuso, normalmente chiamato **software proprietario**, codice e funzionamento li possono vedere solamente chi ne detiene il diritto d'autore, e si viola quindi la legge cercando di condividerlo e non è possibile studiarlo e copiarlo. Il software aperto, chiamato **software open source** o, meglio, **software libero**, può essere copiato, studiato, redistribuito e modificato da chiunque, purché venga redistribuito con queste 4 libertà fondamentali. Questi 4 principi garantiscono anche maggior trasparenza su ciò che fa un software, soprattutto quando gestisce i tuoi dati e li invia in giro per il mondo ✈️

{{< img src="computer-anni-80.webp" alt="Computer anni 80 (rivisto)" source="https://pixabay.com/it/illustrations/space-invaders-8-bit-atari-computer-4761337/" >}}
Mentre negli anni '80, periodo in cui è nato il movimento del software libero, i dati non giravano mezza internet, oggi regalarli a multinazionali private senza alcuna cura dimostra quanto, purtroppo, ci manca una formazione basilare su ciò che è l'informatica, ma soprattutto dimostra che il settore informatico dei dati non si pone problemi in questo sfruttamento, anche se fortunatamente in Europa esiste un regolamento piuttosto stringente sull'uso e trattamento dei dati personali, chiamato [GDPR](https://it.wikipedia.org/wiki/Regolamento_generale_sulla_protezione_dei_dati). Nell'attesa che la politica regolamenti ciò che si può migliorare, noi proponiamo una visione più etica e rispettosa delle persone.

Questa introduzione serve per aggiornarvi sulla direzione in cui stiamo andando come gruppo Linux. Vogliamo andare a fondo della questione **libertà** fino al sistema operativo. Non è sufficiente usare software libero su sistemi operativi non liberi, è un collo di bottiglia inutile. Utilizzando Linux puoi usare più liberamente software e strumenti tecnologici. E Linux non è più un'alternativa che "funzionicchia" come 10 anni fa, anzi, oggi per l'utente medio funziona molto meglio di altri sistemi proprietari come Windows 10 o 11, ed è anche molto più facile e veloce.


### ☁️ ServiziLiberi.it

{{< img src="servizi-liberi.webp" alt="ServiziLiberi.it" source="https://codeberg.org/este-linux/materiale-grafico/src/branch/main/servizi-liberi" >}}

A gennaio nasce [ServiziLiberi.it](/novita/servizi-liberi/), un sito web pratico che mette a disposizione gratuitamente dei software su un nostro server (in "cloud" se ti suona più familiare). Meno i tuoi dati girano per la rete, meglio è, ma visto che chi ha necessità di usare certi strumenti non sempre ha modo di installarli rapidamente, oppure sta usando un telefono o non ha le competenze per fare da sè, li abbiamo predisposti noi. Sono software liberi, per cui chiunque può guardare come funzionano. Sono su un nostro server, e non salvano dati personali perché non li richiedono o, nelle rare occasioni, vengono salvati per il minimo tempo indispensabile per fare ciò per cui sono predisposti.

Tra questi software è possibile trovare uno **strumento per i PDF** contenente più di 50 funzioni, un'applicazione per **gestire le ricette**, un lettore di **feed RSS** (aggregatore di notizie) e un **traduttore multilingua**.

E' stata anche un'**esperienza formativa** sotto molti punti di vista. Anche se sono software scritti da persone diverse in giro il mondo, noi **abbiamo contribuito alle traduzioni in italiano**, così da avere un occhio di riguardo anche all'**inclusività**: non tutte le persone conoscono l'inglese, per cui è importante che i programmi forniti siano tradotti anche in italiano.


### 🪧 I comuni dovrebbero usare altri social network

Un comune può utilizzare, come strumento principale di diffusione delle sue attività, una piattaforma su cui non ha il controllo? A maggior ragione, obbligando le persone a iscriversi e a fornire i propri dati personali ad un'azienda privata, spesso neppure italiana o residente nell'Unione Europea e quindi senza tutela del GDPR. Nel suo ruolo di istituzione pubblica, un comune cerca di usare strumenti che raggiungano il maggior numero di cittadini, e molti sono già iscritti su Facebook, Instagram, TikTok, X, ma non tutti.

Ma che alternative ci sono? Eccome se ci sono 😉! Per questo motivo Fabio si è messo in contatto con un paio di comuni per avviare un esperimento, cioè riuscire a fargli creare un profilo istituzionale su un social network che non è privato, non è di nessuna azienda ed è gestito da volontari, senza pubblicità e senza algoritmi che propongono cosa e quando leggere in base a un'enorme mole di dati personali raccolti.

> Vogliamo portare gli enti pubblici a comunicare su **Mastodon** invece che **Facebook**.

{{< img src="social.webp" alt="Social network e comuni" source="https://draco.pe.kr/archives/11728" >}}

Ce la faremo? Si tratta di un piccolo cambiamento. Non occorre cancellare il profilo dalla piattaforma privata per cambiare drasticamente canale di comunicazione, si possono mantenere entrambi i profili inizialmente. Stiamo fornendo questa **consulenza gratuita** che toglie d'impiccio il comune anche da possibili problemi futuri riguardo il GDPR, oltre che regalargli visibilità per un'iniziativa così etica e originale.

Sembra una cosa troppo complicata, da smanettoni, da informatici? Beh, 🇨🇭 Svizzera e 🇫🇷 Francia hanno già attivato, ciascuno, una loro **istanza di Mastodon per tutti i ministeri** ([fonte Mastodon in Svizzera](https://attivissimo.blogspot.com/2023/09/il-governo-svizzero-sbarca-su-mastodon.html) e [istanza Mastodon del governo francese](https://social.numerique.gouv.fr/explore)), altro che un piccolo profilo utente! 😲


### 🇿 Formazione su Zorin OS a Colle Umberto con il PNLug

{{< img src="ilseste-pnlug.webp" alt="Zorin OS con PNLug" source="fotografie scattate dal PNLug" >}}

Sabato 3 febbraio, **Fabio** e **Albano** si sono diretti a **Colle Umberto** in provincia di Treviso alla **Scuola Primaria G. Pascoli** per dare una mano al [PNLug](https://www.pnlug.it/) (Gruppo Utenti Linux di Pordenone). Il PNLug ha installato Linux nei computer dell'aula di informatica di questa scuola, a livello volontario, per permettere a insegnanti e studenti di utilizzare un sistema operativo libero, con tutti i software liberi che ne derivano. La nostra visita è servita per fare un corso introduttivo, tenuto da Albano, sull'utilizzo di [Zorin OS](https://zorin.com/) (la distribuzione Linux scelta), e al termine Fabio ha presentato brevemente ServiziLiberi.

### 💡 Anticipazione delle prossime attività

🧑‍🏫 A marzo si dovrebbe concretizzare un incontro con gli studenti di 2 istituti tecnici, nelle classi di informatica, dove sarà presentato ServiziLiberi.it per dare spunto ai futuri diplomati su quali siano gli sbocchi nel mondo del lavoro abbracciando la filosofia del software libero, della comunità e della condivisione.

🤖 Ad aprile forse saremo presenti in fiera dell'elettronica e del radioamatori a Pordenone insieme al PNLug.

💭 Stiamo cercando e provando altri software da caricare su ServiziLiberi.it :-)
