---
title: Gestire password e note sicure con ServiziLiberi.it
description: Come inviare password, note o informazioni sicure con auto-distruzione
date: 2024-03-07T22:02:00+02:00
authors:
    - loviuz
slug: servizi-liberi-note-sicure
images:
    - servizi-liberi-note-sicure.webp
layout: news
fullcover: false
tags:
    - ils
    - serviziliberi
    - rss
    - note
    - password
IgnoreTableOfContents: true
---

Proseguono i servizi caricati su [ServiziLiberi](/novita/servizi-liberi) e oggi presentiamo **Note Sicure**, avviabile dalla solita pagina delle applicazioni:

{{< call-to-action title="Note Sicure su ServiziLiberi.it ➡️" url="https://serviziliberi.it/board/Applicazioni" >}}


## Perché?

Sarà capitato sicuramente almeno una volta nella vita che, a casa o a lavoro, una persona ci abbia chiesto di inviargli un'**informazione sensibile** come una password, oppure la coppia completa di **nome utente e password**, o per esempio il numero di **carta di credito** per fare un acquisto "al volo", per poi dimenticare quel dato. Ma chi dovrebbe dimenticare questi dati? Noi sicuramente li dimentichiamo facilmente, ma un dispositivo no. Se usiamo un'applicazione di messaggistica come Whatsapp, Telegram , Signal o altre, con la [cifratura end-to-end](https://it.wikipedia.org/wiki/Crittografia_end-to-end) si ha una certa sicurezza, per cui chi sta in mezzo (il gestore del servizio di messaggistica) non può teoricamente vedere il messaggio scambiato. Ma se il telefono venisse smarrito, rubato o a distanza di tempo, la cifratura end-to-end non avrebbe effetto. E se venisse scoperta una **vulnerabilità** sull'applicazione di messaggistica o sui server del gestore dell'applicazione (e ne vengono scoperte quotidianamente)?

Ancor peggio quando si usano le **email**, soprattutto nel mondo del lavoro, le quali permettono di trasmettere **messaggi in chiaro**, quindi non cifrati, e spesso è abitudine lasciarle archiviate presso il gestore del servizio usando un server IMAP, un archivio esposto in chiaro su internet e sul server di un fornitore che aspetta solo capiti un furto di dati (data leak), perché si sa, capita sempre agli altri, ma prima o poi capita anche a noi! Basta 1 solo accesso abusivo al nostro profilo di posta elettronica per lasciare consultabili a malintezionati tutte le nostre email, dove con una semplice ricerca di "password", "carta", "credito", "accesso" si può trovare di tutto, anche dati che non ricordavamo di aver mandato, perché noi dimentichiamo... ma il dispositivo ricorda tutto.


## Come funziona

E' un'applicazione web che permette di scrivere una nota, una password o qualsiasi altra cosa importante ci passi per la testa, e permette di condividerla con altre persone in sicurezza. Le peculiarità principali sono:
 - **cifratura lato client**: quando viene scritta la nota, al momento della generazione, viene generata una chiave sul tuo dispositivo e non viene assolutamente salvata sul server, che archivia invece solo il dato cifrato. Solamente inviando l'indirizzo generato alla seconda persona questa potrà visualizzarla, perché nell'indirizzo di condivisione è presente anche la chiave di decifratura, oppure si può inviare un indirizzo "pulito" e la chiave tramite un altro canale (per i paranoici della sicurezza)
 - **scadenza nota per numero di visualizzazioni**: si può impostare che la nota si auto-elimini dal server alla prima visualizzazione. E' la scelta più sicura. L'opzione successiva può essere combinata con questa.
 - **scadenza nota per numero di giorni**: si può impostare che la nota si auto-elimini dal server dopo un certo numero di giorni. L'opzione precedente può essere combinata con questa.
 - **aggiunta di una password (facoltativo)**: sempre per i paranoici della sicurezza, oltre alla cifratura lato client è possibile impostare una password per la lettura del contenuto


> ### L'uso più facile e veloce
> Il metodo più diffuso, sicuro e semplice è la condivisione di una nota con la scadenza dopo **1 visualizzazione**, che è anche l'impostazione predefinita. Questo permette di avere anche una sicurezza aggiuntiva, e cioè che la persona ricevente, se può aprire l'indirizzo, significa che dall'invio non l'ha aperto nessun'altra persona terza.

Successivamente, puoi inviare un messaggio o un'email così:

*Ciao Mario,*

*accedi a www_miosito_it con questi dati:*

*UTENTE: mario*

*PASSWORD: https://notesicure.serviziliberi.it/indirizzo_generato_ecc...*

*Attenzione: la password si auto-eliminerà una volta aperta, per cui occorre salvarla in un luogo sicuro o cambiarla al primo accesso*


## Curiosità

L'applicazione si chiama **[Hemmelig](https://hemmelig.app/)**, che in norvegese significa "segreto". Il principale sviluppatore, probabilmente l'ideatore, è infatti norvegese.

E' ovviamente software libero per cui si può verificarne il funzionamento dal loro [repository su Github](https://github.com/HemmeligOrg/Hemmelig.app) e da cui si può contribuire.

Noi abbiamo contribuito con la **traduzione italiana da zero** e abbiamo anche **ampliato la parte di codice traducibile**, rimangono solo le pagine della privacy e dei termini e condizioni d'uso da tradurre interamente.
