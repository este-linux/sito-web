---
title: Amo il Software Libero
description: Giornata del ringraziamento del software libero organizzato da FSFE
date: 2025-02-04T23:30:00+02:00
authors:
    - loviuz
slug: i-love-fs-2025
images:
    - ilovefs.webp
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - fsfe
    - ilovefs
---


## Amo il Software Libero
Dal 2014, **FSFE** ([Free Software Foundation Europe](https://fsfe.org)) promuove la campagna **I Love Free Software** per dire "Grazie" a chi sviluppa e contribuisce al Software Libero. Il giorno di riferimento è S. Valentino, 14 febbraio e giorni limitrofi.

A Este abbiamo pensato di unirci a questa giornata per ringraziare anche noi chi contribuisce a rendere la tecnologia un posto sicuro, creato dalle persone per le persone, in modo trasparente, aperto e condiviso. Oggi più che mai la tecnologia è onnipresente nelle nostre vite, dall'informazione allo svago, a lavoro e a casa, dalle nostre amicizie alle persone più care e intime. Proprio per questo è un dovere mantenere alta l'attenzione e attiva la divulgazione di queste alternative, che in certi versi non sono più tali ma sono diventate uno standard in contrapposizione ai grossi colossi tecnologici.

Ogni anno, "I Love FS" ha un tema, e il tema 2025 è **hidden projects** (progetti nascosti). Esistono, infatti, moltissimi progetti liberi che non sono conosciuti anche se **vengono usati indirettamente tutti i giorni** da chiunque, attraverso internet, un telefono, un pc. Quante persone sanno cos'è **FFMpeg**? Eppure viene usato nella codifica e conversione di quei video che ci piace tanto guardare. E **libssl**? Scommettiamo che non la conoscete, ma ogni sito che navigate la utilizza costantemente per "crittografare" la connessione tra il vostro computer (o telefono) e il sito web, anche poco fa quando avete aperto questa pagina, per permettervi una navigazione sicura.

Ci sono tante soluzioni libere che non conosciamo, eppure le usiamo, e molte di esse sono mantenute da persone volontarie, e oggi le scopriremo e le ringrazieremo!

## Programma
Ci troveremo **sabato 15 febbraio dalle 9:00 alle 18:30**.

Il mattino lo trascorreremo per fare divulgazione di Software Libero e GNU/Linux in centro a Este, poi terremo un pranzo per chi vorrà unirsi e il pomeriggio faremo un incontro divulgativo e di confronto.

## Dove

Il mattino saremo **in centro a Este al mercato**, più o meno qui sotto i portici:
- https://osmapp.org/45.22848,11.65634

Il pomeriggio saremo al **Patronato Ss. Redentore** al primo piano:
- https://osmapp.org/45.22489,11.65693


### Mattino
- 📋 **9:00 - 12:00:** banchetto in centro durante il mercato per divulgazione
- 🍝 **12:00 - 14:30:** pranzo

### Pomeriggio
- 🏠 **14:30 - 15:00:** accoglienza
- 💌 **15:00 - 15:30:** breve presentazione di ILS e FSFE, del software libero e della campagna "I Love FS". Discussione con le persone partecipanti
- 👻 **15:30 - 16:30:** condivisione dei progetti nascosti ("hidden projects")
- 🕹️ **16:30 - 17:30:** quiz e videogiochi con punteggio finale
- 🎁 **17:20 - 17:30:** premiazione con libro a sorpresa!
- 🍹 **17:30 - 18:30:** aperitivo di chiusura e socializzazione


## Come partecipo?

La partecipazione è libera, ma per organizzarla al meglio ti chiediamo di indicarci la tua presenza da qui:

{{< call-to-action title="👉 Iscrizione" url="https://cryptpad.devol.it/form/#/2/form/view/Cbwmn1fIAlftjccJvOmhUhvvRxzMrUNB4wlHjb7GGhw/" >}}


## Domande e risposte

**E' a pagamento?**

No, l'ingresso è gratuito. E' gradita una donazione volontaria, non per il nostro tempo ma per le spese che sosteniamo.

&nbsp;

**E' un evento esclusivo per chi conosce l'informatica e per gente esperta?**

No, puoi partecipare anche se non hai particolare esperienza. Serve voglia di scoprire cosa c'è dentro il "motore" di ciò che usi tutti i giorni. Alcuni temi potrebbero essere approfonditi un po' e quindi tecnici.

&nbsp;

**Devo venire con un computer/portatile?**

Non è necessario, ma lo puoi portare lo stesso così valutiamo quale distribuzione Linux ti installeremo al prossimo incontro :-)

&nbsp;

**Che motivi ho per venire a conoscervi?**

Se pensi che senza Gmail, Facebook, Instagram, X, Chrome, Google, iPhone, Whatsapp, Windows non si possa vivere, dovresti assolutamente venire a conoscerci :-)