---
title: Giga Arena
description: Torneo di Videogiochi Liberi. Scopri un nuovo mondo di videogiochi e di persone!
date: 2023-08-25T14:30:00+02:00
authors:
    - loviuz
slug: torneo-videogiochi-liberi
images:
    - torneo-videogiochi-liberi.webp
layout: news
fullcover: true
tags:
    - videogiochi
    - softwarelibero
    - linux
---


> **IMPORTANTE:** il luogo dell'evento è stato spostato dal Patronato SS. Redentore di Este a **via Rovigo, 51, Este**.

## Cos'è

E' un torneo di videogiochi liberi che faremo in presenza, come ai vecchi tempi con i famosi **LAN Party**, dove ciascuna persona partecipante porterà il suo computer per giocare nella rete locale tramite cavo di rete o WiFi.

Faremo un torneo con almeno questi 2 giochi:
 - [Minetest](https://minetest.org/): un gioco simile a **Minecraft**, ma più espandibile e programmabile
 - [Xonotic](https://xonotic.org/): un classico sparatutto basato sul motore grafico di **Quake** con ben 16 modalità di gioco


{{< quick_summary >}}
- si gioca con il proprio computer portatile (o fisso) a **Este** ~~al Patronato Ss. Redentore~~ in **via Rovigo, 51, Este**
- devi **preinstallare** [Minetest](https://minetest.org/) e [Xonotic](https://xonotic.org/) da casa possibilmente. **Non costano nulla** e sono liberi
- se non hai un portatile contattaci su [Telegram](https://t.me/openit_este) o per [email](mailto:estelinux@mailbox.org) che te lo diamo noi
- si gioca **dalle 14:30 alle 18:30** circa
- arriva un po' prima 🙂
{{< /quick_summary >}}


## Perché

L'evento ha lo scopo di far conoscere un **mondo alternativo di videogiochi**.

Sono videogiochi liberi nel senso che non hanno scopo di lucro, non sono gestiti da un'azienda per fare profitto ma, piuttosto, da una comunità di persone che li sviluppano e ci mettono tutto il loro impegno per realizzare videogiochi con delle fondamenta etiche e libere. I videogiochi menzionati, ad esempio, non sono gratuiti all'inizio per poi **chiederti soldi per sbloccare oggetti** durante il gioco, creando una sorta di dipendenza, ma funzionano con dinamiche più "sane".

Comunque, il 1° giugno 2023, abbiamo tenuto [una serata di introduzione che puoi rivedere da qui](/novita/videogiochi-liberi/) riguardo i Videogiochi Liberi e perché meritano di essere approfonditi e giocati rispetto ai classici giochi chiusi e commerciali.


## Come fare per partecipare

Occorre iscriversi da Mobilizon:

https://mobilizon.it/events/b380ab98-8c84-4475-adcf-6aa81a2229b3

> Suggerimento: premi **Partecipa** e conferma senza registrarti, in questi giorni stanno risolvendo un problema del servizio.

Per qualsiasi problema di iscrizione tramite Mobilizon o per domande scrivici a **estelinux(@)mailbox.org**.


**L'evento è libero e gratuito.**


## Dove

L'evento è organizzato in presenza nella città di Este in provincia di Padova (Veneto), presso **DevCode Srl in via Rovigo, 51** ~~il Patronato Ss. Redentore in viale Fiume, 65~~:

[📍 Vai alla mappa](https://www.qwant.com/maps/place/osm:node:6739047763@Devcode_srl#map=16.50/45.2197409/11.6662473)


## Domande frequenti

**D:** Devo essere iscritto alla vostra associazione o da qualche parte per partecipare?

**R:**  No, l'ingresso è libero. Consigliamo caldamente l'iscrizione da Mobilizon (vedi sopra) per sapere in quanti saremo, per organizzare al meglio le postazioni.

Tuttavia l'evento ha un costo per l'associazione, per cui puoi lasciare una donazione quando verrai a trovarci o puoi iscriverti a ILS per sostenerci e seguire le nostre attività, ma è assolutamente facoltativo e ci puoi pensare anche più avanti.

--

**D:** Devo acquistare i giochi menzionati per partecipare? Devo per caso avere Linux o posso venire anche con Windows o Mac?

**R:** Non devi acquistare nulla. Minetest e Xonotic sono software libero, per cui li puoi scaricare liberamente e gratuitamente dai relativi siti internet www.minetest.org e www.xonotic.org e installarli. Consigliamo piuttosto di prepararli già installati sul proprio pc e di verificare che il computer sia adeguato per giocarvi, così da assicurarti di poter partecipare giocando in maniera fluida e dando il meglio!

--

**D:** Posso giocare con il gamepad?

**R:** Certamente, anche se con queste tipologie di videogiochi sicuramente andrai meglio con mouse e tastiera :-)

--

**D:** Il computer fisso è ingombrante e non ho un portatile. Come posso fare?

**R:** Se non hai un portatile possiamo procurarlo, ma dobbiamo saperlo per tempo. Scrivici su [Telegram](https://t.me/openit_este) o per [email](mailto:estelinux@mailbox.org)


## Materiale promozionale

Locandina realizzata con [Inkscape](https://inkscape.org/it/).

La grafica con cui è rilasciata la locandina è [Creative Commons CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/deed.it), per cui è liberamente scaricabile, riutilizzabile e adattabile alle proprie esigenze da qui:

https://codeberg.org/este-linux/materiale-grafico/src/branch/main/locandine/eventi/20230923%20-%20torneo%20videogiochi


## Foto evento

Puoi trovare le foto dell'evento qui su Mastodon:

https://mastodon.uno/@openiteste/111159722513858649