---
title: Sabato 18 novembre 2023, Coderdojo a Casale di Scodosia
description: Impara a programmare giocando!
date: 2023-11-01T13:02:00+02:00
authors:
    - loviuz
slug: coderdojo-novembre-2023
images:
    - coderdojo.jpg
layout: news
fullcover: true
tags:
    - coderdojo
    - scratch
    - iniziative
    - bambini
---

# Sabato 18 novembre 2023 torna il **Coderdojo**.

## Impara a programmare giocando!

CoderDojo è un evento gratuito rivolto ai bambini e bambine dai 7 ai 14 anni per avvicinarli in modo divertente all'informatica e in particolare alla programmazione.

L'idea alla base è che i calcolatori, programmandoli, possono essere usati in modo creativo senza subirli passivamente.

Per ulteriori informazioni su CoderDojo potete consultare https://coderdojo.org.

## Cosa portare

I bambini e le bambine dovranno essere accompagnati da un genitore che possa affiancarli durante tutta la durata dell'evento e portare una merenda per l'intervallo che faremo a metà sessione. E' necessario portare il proprio computer portatile.

## Cosa faremo

Lo scopo del primo incontro sarà creare dei semplici videogiochi, utilizzando un programma sviluppato appositamente per apprendere i concetti fondamentali della programmazione in modo semplice e divertente. Il programma in questione si chiama Scratch, è software libero e disponibile gratuitamente per tutti i principali sistemi operativi (Windows, Linux, Mac).

## Dove

Presso Sala Comunale in Piazza Matteotti, 37 - Casale di Scodosia (PD)

## Come iscriversi

La capienza dell'aula è limitata, se intendete partecipare dovete iscrivervi indicando le generalità dei partecipanti (figlio/a o figli e genitore) e l'età inviando un'email a sculdascialab(@)proton.me

**⚠️ ATTENZIONE: abbiamo raggiunto già 16 partecipanti, limite massimo dell'aula. Scriveteci comunque per la prossima giornata!**

## [👉🏻 **Scarica la locandina**](https://codeberg.org/este-linux/materiale-grafico/src/branch/main/locandine/eventi/2023-11-18%20Coderdojo)

## Giochi creati

Aggiornamento post-evento: abbiamo raccolto i giochi che 5 bambini hanno voluto condividere in rete, li trovate qui sotto il nome **Coderdojo 18_11_2023** con il loro nome:

https://scratch.mit.edu/users/estelinux/projects/

---

**Organizzato da:**

![ILS Este](/images/ilseste.webp)


**In collaborazione con:**

![Sculdascia Lab](sculdascialab.webp)
