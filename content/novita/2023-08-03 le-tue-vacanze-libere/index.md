---
title: 🎧 Le tue vacanze libere
description: I libri fanno viaggiare, ma se devi farlo realmente è meglio avere un navigatore (libero).
date: 2023-08-03T23:20:00+02:00
IgnoreTableOfContents: true
authors:
    - loviuz
slug: le-tue-vacanze-libere
images:
    - le-tue-vacanze-libere.webp
layout: news
fullcover: false
tags:
    - podcast
    - openstreetmap
    - libri
    - vacanze
---

## Ascolta il mare... di novità

Siamo prossimi alle ferie estive per cui vogliamo proporvi quali strumenti usare per farvi guidare nelle vostre vacanze, partendo dal navigatore fino ai libri. Ovviamente non vogliamo proporvi di svendere i vostri dati anche in vacanza a ogni tocco sul telefono, e di regalare quindi a Google o altre grosse aziende tecnologiche i vostri itinerari per meglio profilarvi, ma vi proponiamo applicazioni e servizi liberi da qualsiasi logica di tracciamento e di profilazione. E, sinceramente, questa volta vi stupiremo per la loro semplicità e qualità.


Buon ascolto!

{{< rawhtml >}}
<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://funkwhale.it/front/embed.html?&amp;type=track&amp;id=12693"></iframe>
{{< /rawhtml >}}


## Mappe libere

Abbiamo proposto principalmente [Organic Maps](https://organicmaps.app/) come app che funge da navigatore, con queste caratteristiche:
- funziona su Android e iOs
- mappe offline
- funge da navigatore
- guida vocale
- avviso di autovelox
- avviso di attraversamento pedonale
- percorsi ciclabili, sentieri escursionistici e percorsi pedonali
- rispetta la tua privacy (chiede solo l'accesso alla geolocalizzazione rispetto ad altre app che chiedono l'accesso a rubrica, fotocamera, file, ecc)
- risparmia la batteria
- nessun addebito imprevisto sui dati mobili perché è offline
- non ha pubblicità o traccianti e non manda email invadenti perché non chiede di registrarsi
- si possono lasciare delle note ai mappatori per far aggiungere o correggere punti o strade.

Altri progetti interessanti:
- Ristoranti, bar e locali vegani o vegetariani:
  https://veggiekarte.de/?lang=it

- Ristoranti, bar e locali per celiaci (intolleranza al glutine):
  https://umap.openstreetmap.fr/it/map/locali-per-celiaci_927207

- Mappa distributori con prezzi aggiornati:
  https://umap.openstreetmap.fr/it/map/distributori-prezzi-italia_932492

- Meteo:
  https://open-meteo.com



## Libri

I principali formati dei lettori di e-book che troviamo sul mercato sono:
 - **Kindle**: gestito da Amazon (Kindle unlimited / Prime Reading), puoi leggere solo libri dal loro negozio virtuale (commerciale)
 - **Kobo**: promosso da La Feltrinelli ma non vincolato a loro, infatti si possono caricare libri in formati aperti come ePub, PDF, ecc (commerciale)
 - altri lettori di e-book senza negozio virtuale sono: **Sony/Pocketbook/OnyxBook/Tolino**
 - **ReMarkable2**: quaderno digitale (commerciale)
 - **[PineBook](https://www.pine64.org/pinebook/)**: per smanettorni, progetto libero

Oltre ai lettori di libri digitali, ci sono vari siti contenenti libri digitali senza diritto d'autore:
 - [LiberLiber](https://liberliber.it/) (italiano)
 - [LibriVox](https://librivox.org/) (italiano e inglese)
 - [Progetto Gutenberg](https://www.gutenberg.org/) (multilingua)
 - [BookRepublic](https://www.bookrepublic.it/) (italiano)
