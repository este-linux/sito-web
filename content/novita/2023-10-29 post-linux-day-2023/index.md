---
title: Dopo il Linux Day 2023
description: Un evento che smuove sempre le acque per far riscoprire un mondo digitale alternativo e libero
date: 2023-10-29T13:07:00+02:00
authors:
    - loviuz
slug: post-linux-day-2023
images:
    - post-linux-day.jpg
layout: news
fullcover: true
tags:
    - linux
    - linuxday
    - iniziative
---

## Come è andata?

**Sabato 28 ottobre** 2023 si è tenuto il [Linux Day](/novita/linux-day-2023).

Come previsto, hanno partecipato le classi 4^a e 5^a di informatica dell'[IIS Euganeo di Este](https://www.iiseuganeo.cloud/), mentre il pomeriggio si è tenuto un incontro senza presentazioni e più aperto al Patronato SS. Redentore per sperimentare, discutere e condividere esperienze.

![Linux Day 2023 a Este, il pomeriggio](linux-day-este-pomeriggio.webp)

Siamo stati molto contenti perché, essendo un gruppo ancora abbastanza giovane nel territorio, ci hanno fatto visita persone nuove. Sappiamo che il tema di Linux, del software libero e i rischi di un mondo digitale non libero sono temi per chi ha già determinate sensibilità, infatti c'è stata qualche difficoltà a coinvolgere diversi studenti che non conoscono. 

![Fabio Lovato, introduzione a GNU/Linux e digitale](fabio-lovato-linux.webp)

L'introduzione di **Fabio Lovato** ha posto le basi per addentrarsi nei problemi attuali, come l'esistenza di molti software e app equivalenti a **grandi scatole nere** con un enorme pulsante sopra, di cui non sappiamo cosa avviene all'interno. La stessa cosa vale per i servizi in cloud che funzionano allo stesso modo, con la differenza che il programma non viene nemmeno eseguito nel nostro dispositivo ma su quello di qualcun'altro.

![Carlo Baratto, storia dell'informatica](carlo-baratto-storia-informatica.webp)

E' stata importante infatti la presentazione di **Carlo Baratto** che ha illustrato **la storia dell'informatica dagli anni '60** per capire come mai siamo arrivati oggi, nel 2023, con queste tecnologie e quale è stato il percorso. L'**intelligenza artificiale** non è nata lo scorso anno con ChatGPT, ma già dagli anni '70 c'erano i primi prototipi, se pur diversi dagli attuali. Molto interessante anche la seconda presentazione che promuove l'uso di telefoni con sistema operativo libero, ma soprattutto è stato utile conoscere la filosofia del riuso poiché esistono telefoni con batteria sostituibile autonomamente (così com'era in origine), ma anche schermo, fotocamera, connettore di ricarica e altra componentistica. Stiamo sommergendo Paesi del Terzo Mondo di rifiuti tecnologici, dobbiamo trovare un metodo più etico nell'utilizzarli e di non cambiare dispositivo così frequentemente, ma anzi di usarne di riparabili e con un occhio di riguardo al software all'interno per combattere l'obsolescenza programmata e migliorare la "pesantezza" dei software forniti.

![Albano Battistella, Zorin OS](albano-battistella-zorin-os.webp)

Oltre a elencare i problemi e i rischi si è parlato anche di soluzioni, come per esempio la presentazione di **Albano Battistella** riguardo il software libero e la scuola dove ha mostrato **Zorin OS** (di cui è contributore), un sistema operativo GNU/Linux semplicissimo da usare e alla portata di tutti e tutte. Ha raccontato, infatti, che è stato installato in molte scuole nei progetti che ha seguito personalmente, per aiutare questi istituti nella migrazione da Windows (software chiuso) a GNU/Linux (software libero e aperto), con la riduzione di vari problemi legati ai dati personali degli studenti e con un notevole risparmio di costi di licenza.

![Andrea Bordin, GDPR](andrea-bordin-gdpr.webp)

Breve ma intensa la panoramica sul **GDPR** di **Andrea Bordin** che ha portato esempi e domande su come vengono trattati i nostri dati con la normativa europea in vigore ormai dal 2016 anche in Italia. La tecnologia cresce a ritmi sostenuti, mentre i governi e le istituzioni non riescono a stare al passo con i tempi per discutere e porre delle limitazioni o creare norme per le tecnologie più pericolose, per cui sta al cittadino, ai volontari e alle associazioni sensibili su questi temi cercare di smuovere l'opinione pubblica e proporre soluzioni che aiutino a proteggere meglio i nostri dati, con spirito critico e costruttivo.


## Come seguirci

Vi invitiamo a seguirci sui nostri canali:

- su **[Telegram: ILS Este - Notizie](https://t.me/openit_este_news)**. Canali di sole notizie, senza interazione
- su **[Telegram: ILS Este - Gruppo](https://t.me/openit_este)**. Principale canale di discussione
- su **[Matrix](https://app.element.io/#/room/!FQaIJQacPkLZHJaMfQ:matrix.org)**: vengono sincronizzati i messaggi fra il gruppo Telegram e Matrix


## Presentazioni e materiale

Il materiale utilizzato verrà pubblicato a breve su questa pagina qui sotto:

- ✅ **Linux e software libero: apriamo Internet**: [scarica](https://codeberg.org/este-linux/presentazioni/src/branch/main/2023-10-28%20linux%20day%202023/Intro%20Linux%20Day%202023.pdf) - [sorgenti](https://codeberg.org/este-linux/presentazioni/src/branch/main/2023-10-28%20linux%20day%202023/Intro%20Linux%20Day%202023-sorgenti)
- ✅ **40 anni di storia dell’informatica in 30 minuti**: [scarica](https://codeberg.org/este-linux/presentazioni/src/branch/main/2023-10-28%20linux%20day%202023/Slide%20Linux%20Day%20Storia.pdf)
- ✅ **Software libero per una scuola libera**: [scarica](https://codeberg.org/este-linux/presentazioni/src/branch/main/2023-10-28%20linux%20day%202023/Zorin-OS-Brochure-italiano-28.10.2023.pdf)
- ✅ **Linux e software libero: riprendiamoci i dati**: [video "Una vita senza Big Tech"](https://peertube.uno/w/cmoY3DUvbeDE8JN7DUjJWL)
- ✅ **I nostri dati sono al sicuro in Europa? Il GDPR**: [scarica](https://codeberg.org/este-linux/presentazioni/src/branch/main/2023-10-28%20linux%20day%202023/Privacy.odp)
- ✅ **Fairphone e telefoni liberi: filosofia del riuso e dell’uso software libero**: [scarica](https://codeberg.org/este-linux/presentazioni/src/branch/main/2023-10-28%20linux%20day%202023/Slide%20Linux%20Day%20-%20Telefoni.pdf)
