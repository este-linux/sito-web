---
title: Linux Day 2024
description: L'anno del pinguino? Scopri come riprendere la sovranità digitale
date: 2024-09-22T18:39:00+02:00
authors:
    - loviuz
slug: linux-day-2024
images:
    - linuxday_fullcolor.jpg
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - linuxday
    - iniziative
---


**Sabato 26 ottobre** 2024 torna il [Linux Day](https://www.linuxday.it/), l'evento italiano per promuovere **Linux** e il **Software Libero**.

In questo evento ci sarà la possibilità di (ri)scoprire cos'è Linux, i passi da gigante fatti in tutti questi anni e ci sarà modo di provarlo e installarlo sui vostri computer!

Non hai un computer? Nessun problema, c'è modo di "liberare" anche il tuo telefono se è Android, ci sono tantissime alternative libere per riprendere il controllo del tuo telefono e **vivere in un mondo digitale assieme a questa grande comunità del software libero**.


Aperto a chiunque dalle 14:30 alle 18:30, liberamente e gratuitamente, al **Patronato SS. Redentore di Este in viale Fiume, 65**:

| Orario            | Argomento                                                 |
|:-----------------:|-----------------------------------------------------------|
| **14:30 - 18:30** | Installazione di Linux, laboratorio, dimostrazioni     |

Bar aperto 🍻🥪


Anche quest'anno dedicheremo il **mattino** per una sessione dedicata alla scuola, questa volta all'[IIS J.F. Kennedy di Monselice (PD)](https://www.iiskennedy.edu.it/), con argomenti specifici per la scuola. Da qui si forma il futuro delle nuove generazioni che non devono subire la tecnologia ma devono saperla gestire e scegliere in modo consapevole che cosa usare, e per chi ha più tenacia, può scoprire come contribuire attivamente al cambiamento.

_Programma in fase di definizione..._


## Partecipa

Per gestire al meglio le sale consigliamo di indicare la partecipazione da Mobilizon:

https://mobilizon.it/events/75a54397-f8c4-427b-92b1-15c298e42627

Non serve iscriversi, si può confermare anche solo inserendo il proprio nome e email per conferma, non verranno inviate pubblicità o usate in modo improprio.
