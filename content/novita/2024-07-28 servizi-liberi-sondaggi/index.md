---
title: Sondaggi in semplicità su ServiziLiberi.it
description: Come creare sondaggi per incontri o voti in pochi passi
date: 2024-07-28T16:23:00+02:00
authors:
    - loviuz
slug: servizi-liberi-sondaggi
images:
    - servizi-liberi-sondaggi.webp
layout: news
fullcover: false
tags:
    - ils
    - serviziliberi
    - sondaggi
    - incontri
    - appuntamenti
IgnoreTableOfContents: true
---

Prima di prenderci una pausa estiva, dopo una settimana di test pubblichiamo su [ServiziLiberi](/novita/servizi-liberi) un nuovo servizio per i **Sondaggi**:

{{< call-to-action title="Sondaggi su ServiziLiberi.it ➡️" url="https://serviziliberi.it/board/Applicazioni" >}}


## Perché?

Tra amici, amiche, lavoro o famiglia, a volte serve decidere qualcosa insieme quando si è in più persone. Vediamo messaggi di questo tipo:
 - **tu:** "Ci troviamo allora sabato sera alle 21? 😃"
 - **Mario:** "ok! 👍"
 - **Federica:** "io non posso, facciamo alle 22? 😕"
 - **Stefano:** "io andrei meglio domenica, sabato non posso. Chi può domenica? 🙄"
 - **Alessandra:** "Mo avete rotto... 😡 però... io farei lunedì! Però non alle 21"

A lavoro la chat sarà magari più formale di questa sopra, ma il problema rimane lo stesso. Come fare quindi?

Con un semplicissimo sondaggio, che si può creare in meno di un minuto, si propongono date e orari, e ciascuna persona vota! Poi magari non tutte le persone usano la stessa app di messaggistica, per cui meglio mandare un link e appoggiarsi a un collegamento esterno in certi casi.


## Come funziona

E' un'applicazione web che permette di creare un sondaggio in 3 modalità:
 - data-ora: serve per votare una data o ora di un appuntamento o un incontro
 - data: come sopra, ma per scegliere solamente una data
 - sondaggio normale: domanda e più risposte votabili

E' possibile creare un profilo per gestire i propri sondaggi, ma se non serve tenere un archivio di sondaggi importanti è sufficiente crearlo senza creare alcun profilo, per cui funzionerà in modalità usa-e-getta. Durante la creazione del sondaggio è possibile scegliere alcune impostazioni avanzate, come:
 - permettere voti anonimi: chiunque abbia il link può votare, scrivendo un nome qualsiasi
 - richiedere di accedere con un utente per votare (non anonimo)
 - votare solo su invito
 - permettere i commenti
 - visualizzare il sondaggio nella lista dei sondaggi pubblici
 - permettere i cambiamenti dei voti anonimi
 - permettere un solo voto per utente
 - mostrare gli inviti come voti vuoti
 - non ammettere scelte vuote
 - nascondere i partecipanti
 - mostrare il punteggio invece della percentuale

## Curiosità

L'applicazione si chiama **[BitPoll](https://bitpoll.de/)**.

E' software libero per cui si può verificarne il funzionamento dal loro [repository su Github](https://github.com/fsinfuhh/Bitpoll/) e da cui si può contribuire.

E' stato sviluppato dagli **studenti del Dipartimento di Informatica dell'Università di Amburgo**. L'obiettivo era quello di utilizzarlo come uno dei servizi forniti su [mafiasi.de](https://mafiasi.de), un portale di servizi per altri studenti e studentesse.

Noi abbiamo contribuito con la **traduzione italiana da zero**.

Buone vacanze estive! 😎⛱️