---
title: "Docker: parte 2"
description: Impariamo a usare docker per creare servizi web liberi!
date: 2024-12-01T15:06:00+02:00
authors:
    - loviuz
slug: docker-2024-2
images:
    - incontro_docker.jpg
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - docker
    - serviziliberi
    - self-hosting
---


Dopo l'[introduzione a Docker di sabato 2 novembre](/novita/docker-2024), concluderemo **sabato 7 dicembre dalle 14:30 alle 18:30** con:
 - reverse proxy per pubblicare in rete uno o più servizi su container
 - sistema di backup
 - container (o altro sistema) per fare da firewall software e logging
 - log e debug per trovare eventuali errori


## Programma

Aperto a chiunque dalle 14:30 alle 18:30, liberamente e gratuitamente, al **Patronato SS. Redentore di Este in viale Fiume, 65**:

| Orario            | Argomento                                                 |
|:-----------------:|-----------------------------------------------------------|
| **14:30 - 15:00** | 🤗 Accoglienza     |
| **15:00 - 18:00** | 🔮 Laboratorio pratico e sperimentazione     |
| **18:00 - 18:30** | 👋 Rilascio attestati finali (pacca sulla spalla molto più empatica)     |

Bar aperto 🍻🥪

## Partecipa

E' gradito indicare la partecipazione qui:

https://mobilizon.it/events/11fc7951-5d7b-4806-8743-1fbf7c1bd4e9

Non serve registrarsi, si può confermare anche solo inserendo il proprio nome e email per conferma, non verranno inviate pubblicità o usate in modo improprio.

