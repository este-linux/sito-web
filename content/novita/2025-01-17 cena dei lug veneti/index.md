---
title: "Cena dei LUG Veneti a Monselice"
description: "Cos'è e come è andata una tradizione da 20 anni"
date: 2025-01-25T17:13:00+02:00
authors:
    - loviuz
slug: cena-dei-lug-veneti-2025
images:
    - photo_2025-01-17_21-02-53.webp
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - lug
    - veneto
---


## Un po' di storia
Nel lontano 2005, il **MonteLUG** (Gruppo Utenti Linux di Montebelluna) organizza [la prima cena dei LUG Veneti](https://wiki.montellug.it/?title=Categoria:Cena_dei_LUG). E' una cena dove ritrovarsi fra tutti i gruppi Linux in Veneto e **viene organizzata tassativamente a ogni ricorrenza di venerdì 17**. Dal wiki di MonteLUG:

> Ogni venerdì 17 che si rispetti ha la sua cena dei LUG veneti, oramai è una tradizione e ci teniamo a rispettarla. La data non viene scelta, come sostiene qualche voce di corridoio, perché gli informatici non siano superstiziosi, bensì per il motivo opposto: il vero nerd crede in una sola regola immutabile, la famigerata "legge di Murphy". Ciò comporta che:
>     il server, che ha sempre funzionato perfettamente durante tutti i test, va sistematicamente a farsi benedire quando è ora di entrare in produzione;
>    un programma pronto da dare al cliente presenterà immancabilmente un bug misterioso il giorno della presentazione al committente che farà fare (nel caso migliore) una pessima figura davanti a tutti. Tale bug inoltre non si ripresenterà più nei giorni seguenti quando si tenterà invano di stanarlo;
>    gli esempi si possono facilmente estendere ad altri settori dell'informatica...

> Per qualche strano motivo i venerdì 17 la legge di Murphy sospende il suo effetto e i nerd escono allo scoperto dalle loro sale CED per qualche ora d'aria senza doversi preoccupare di cosa succederà ai loro server o a tutte le calamità possibili che possono succedere al sistema di backup.

> Almeno così narra la leggenda... 

Ed è così che il nostro gruppo ha voluto farsi avanti per organizzare la prima ricorrenza del 2025 che cadeva a gennaio, venerdì 17 ovviamente!

Gente da vari paesi, da Montebelluna passando per Mestre, Treviso, un po' qua e un po' là.

In totale eravamo 26 persone, abbiamo cenato insieme, per la maggior parte siamo andati e andate di pizza, e si è chiacchierato molto. Non è da sottovalutare la semplice "chiacchierata" senza computer e senza dover "fare" per forza qualcosa, tante volte basta solo vedere facce nuove, sentire che tizio fa questo, caio fa quest'altro, condividere qualche esperienza, conoscere qualche persona nuova che hai di fronte e magari non avevi mai visto, e viene subito voglia di fare qualcosa anche a te :-)

Qui la cronologia di tutte le cene precedenti: [https://wiki.montellug.it/?title=Categoria:Cena_dei_LUG](https://wiki.montellug.it/?title=Categoria:Cena_dei_LUG)

Alla prossima, che sarà venerdì 17 ottobre 2025! 👋

{{< img src="photo_2025-01-17_22-04-08.jpg" title="Cena dei LUG Veneto 17/01/2025" alt="Cena dei LUG Veneto 17/01/2025" author="Michele Bertoncello" license-text="CC BY SA 4.0" license-url="https://creativecommons.org/licenses/by-sa/4.0/deed.it" >}}
