---
title: "Docker: crea il tuo server libero"
description: Impariamo a usare docker per creare servizi web liberi!
date: 2024-11-01T15:06:00+02:00
authors:
    - loviuz
slug: docker-2024
images:
    - incontro_docker.jpg
layout: news
fullcover: true
IgnoreTableOfContents: false
tags:
    - linux
    - docker
    - serviziliberi
    - self-hosting
---


Ogni primo sabato del mese ci troviamo al Patronato Ss. Redentore per sperimentare e per condividere esperienze sul mondo digitale e per provare o cercare alternative libere agli strumenti di tutti i giorni. Questo sabato però è diverso... 🤓

**Sabato 2 novembre** 2024 impareremo a usare **Docker**.

## Cos'è Docker?

Immagina di avere una cucina molto piccola, dove prepari i tuoi piatti preferiti. Ogni volta che vuoi cucinare qualcosa di nuovo, devi tirare fuori tutti gli ingredienti e gli utensili, e poi pulire tutto dopo. È un po' complicato, vero?

Ora, pensa a Docker come a una serie di scatole magiche che ti aiutano a cucinare senza disordine. Ogni scatola contiene tutto ciò che ti serve per preparare un piatto: ingredienti, ricette e utensili. Così, quando vorrai cucinare un piatto, dovrai prendere semplicemente la scatola corrispondente, aprirla e voilà! Puoi iniziare a cucinare senza dover cercare in giro per la cucina.

In termini tecnici, Docker è un programma che permette a chi sviluppa software di **"impacchettare"** le applicazioni e tutto ciò di cui si ha bisogno per farle funzionare in un ambiente chiamato "contenitore". Questo significa che possono portare le loro applicazioni ovunque, proprio come tu puoi portare le tue scatole di ingredienti a casa di un'amica o amico e cucinare senza problemi.

Docker è come avere una cucina organizzata con scatole magiche che rendono tutto più facile!

## A chi è rivolto

A chi vuole creare una serie di servizi in casa, il cosiddetto **self-hosting**, per sè stessi o per una comunità, come un sistema di gestione foto o file, in stile Google Drive, ma con software libero e auto-gestito, oppure un sito personale ma sempre auto-gestito, o per esigenze a lavoro. E' l'idea alla base di [ServiziLiberi](https://serviziliberi.it).

Sono necessarie alcune conoscenze base di reti (almeno cos'è un indirizzo IP e una rete), e un minimo di dimestichezza con alcuni comandi su Linux da terminale.


## Programma

Aperto a chiunque dalle 14:30 alle 18:30, liberamente e gratuitamente, al **Patronato SS. Redentore di Este in viale Fiume, 65**:

| Orario            | Argomento                                                 |
|:-----------------:|-----------------------------------------------------------|
| **14:30 - 15:00** | 🤗 Accoglienza     |
| **15:00 - 16:00** | 🎓 Teoria e esempio pratico     |
| **16:00 - 18:00** | 🔮 Laboratorio pratico e sperimentazione     |
| **18:00 - 18:30** | 👋 Rilascio attestati (sotto-forma di pacca sulla spalla)     |

Bar aperto 🍻🥪

## Partecipa

E' gradito indicare la partecipazione qui:

https://mobilizon.it/events/1f3cebfb-7778-441f-a3d6-fc76b4722c6e

Non serve registrarsi, si può confermare anche solo inserendo il proprio nome e email per conferma, non verranno inviate pubblicità o usate in modo improprio.


## Evento

L'evento è terminato come previsto. Eravamo in 8 persone in presenza + 2 da remoto. E' stato apprezzato e seguito con piacere. Abbiamo avuto modo di provare Docker anche su macOS e funziona.

[![DSC_0066.JPG](DSC_0066.JPG)](DSC_0066.JPG)
[![DSC_0067.JPG](DSC_0067.JPG)](DSC_0067.JPG)
[![DSC_0068.JPG](DSC_0068.JPG)](DSC_0068.JPG)
[![DSC_0070.JPG](DSC_0070.JPG)](DSC_0070.JPG)
[![5852841343049647033.jpg](5852841343049647033.jpg)](5852841343049647033.jpg)

La presentazione è scaricabile in formato HTML da:

https://codeberg.org/este-linux/presentazioni/src/branch/main/2024-11-02%20docker

Consultabile direttamente da qui, anche da telefono:

https://este.linux.it/varie/presentazioni/2024-11-02%20docker/


## Appunti dell'evento

Scaricamento e avvio di **librespeed**:
```bash
(sudo) docker run -p 80:80 -d --name speedtest --rm ghcr.io/librespeed/speedtest
```

Visualizzazione container in esecuzione:
```bash
(sudo) docker ps
(sudo) docker ps -a   #Per visualizzare i container arrestati
```

Stop container di librespeed:
```bash
(sudo) docker stop speedtest
```

**docker compose** o **docker-compose**: funzionano allo stesso modo, ma in base alla versione di docker in uso potreste avere il client docker che supporta il comando *compose*, mentre se non funziona occorre installare il pacchetto *docker-compose*.

```bash
sudo apt install docker-compose
```

**Installazione di Portainer**

Istruzioni disponibili da https://docs.portainer.io/start/install-ce

Versione rapida:
```bash
(sudo) docker volume create portainer_data   #Creazione volume per configurazione persistente
```

Avvio di Portainer (Portainer è a sua volta un container):
```bash
(sudo) docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce:2.21.4
```

Aprire da browser: [https://localhost:9443](https://localhost:9443)

- bisogna accettare il certificato di sicurezza autogenerato (e continuare con l'eccezione di sicurezza)
 - creare una password per l'utente Portainer
 - appena entrati selezionare *Get Started* e la versione locale di Docker (cliccando sul riquadro con il nome)

Spiegazione di alcuni comandi base:
- *docker pull*: scarica solo l'immagine
- *docker run*: avvia un nuovo container e, se necessario, scarica l'immagine (pull)
- *docker start*: avvia un container già creato

**Configurazioni dentro Portainer**

In **Network List**:
 - driver *bridge* (predefinito) permette di mettere in bridge la rete dei container docker con altri pc della stessa rete
 - driver *host* permette al container di non essere raggiunto al di fuori dalla macchina host

**Env** sono le variabili d'ambiente.

**Restart policy**: sono le politiche di riavvio. Di solito si imposta *Unless stopped* (a meno che non fermato), la quale permette di lasciare il container in esecuzione sempre finché non viene fermato, altrimenti con *always* va sempre anche se fermato.

Il tasto *Deploy* avvia il container (come *docker run*).

La rete *bridge* predefinita non accetta configurazioni di IP, meglio crearsi la propria rete (network).

In *Containers* è possibile premere *Duplicate/edit* per modificare la configurazione di un container attivo e dando lo stesso nome al container si può premre *Replace* (sostituisci) per modificare al volo il container (funzione originariamente non presente su Docker).

**Watchtower** è un container che controlla aggiornamenti degli altri container e li aggiorna automaticamente.

**Nginx Proxy Manager** è un *Reverse proxy* per esporre i container pubblicamente (è a sua volta un container). Per ServiziLiberi.it usiamo Nginx Proxy Manager Plus (NPMPlus).

**Immagini docker interessanti:**

https://github.com/awesome-selfhosted/awesome-selfhosted#awesome-selfhosted

https://selfh.st/apps

https://hub.docker.com

Grazie a **Damiano (Damtux)** per le note!