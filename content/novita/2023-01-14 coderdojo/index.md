---
title: Sabato 14 gennaio 2023, Coderdojo a Casale di Scodosia
description: Impara a programmare giocando!
date: 2022-12-19T23:21:00+02:00
authors:
    - loviuz
slug: coderdojo-14-gennaio-2023
images:
    - coderdojo.jpg
layout: news
fullcover: true
tags:
    - coderdojo
    - scratch
    - snap!
    - iniziative
    - bambini
---

# Sabato 14 gennaio 2023 torna il **Coderdojo**.

## Impara a programmare giocando!

CoderDojo è un evento gratuito rivolto ai bambini e bambine dai 7 ai 14 anni per avvicinarli in modo divertente all'informatica e in particolare alla programmazione.

L'idea alla base è che programmandoli, i calcolatori possono essere usati in modo creativo senza subirli passivamente.

Per ulteriori informazioni su CoderDojo potete consultare https://coderdojo.org e https://coderdojoitalia.org.

## Cosa portare

I bambini e le bambine dovranno essere accompagnati da un genitore che possa affiancarli durante tutta la durata dell'evento e portare una merenda per l'intervallo che faremo a metà sessione. E' necessario portare il proprio computer portatile.

## Cosa faremo

Lo scopo del primo incontro sarà creare dei semplici videogiochi, utilizzando un programma sviluppato appositamente per apprendere i concetti fondamentali della programmazione in modo semplice e divertente. Il programma in questione si chiama Scratch (oppure anche Snap!), sono software liberi e disponibili gratuitamente per tutti i principali sistemi operativi (Windows, Linux, Mac).

## Dove

Presso Sala Comunale in Piazza Matteotti, 37 - Casale di Scodosia (PD)

## Come iscriversi

La capienza dell'aula è limitata, se intendete partecipare dovete iscrivervi indicando le generalità dei partecipanti (figlio/a o figli e genitore) e l'età inviando un'email a sculdascialab(@)proton.me

**⚠️ ATTENZIONE: abbiamo raggiunto già 15 partecipanti, limite massimo dell'aula. Scriveteci comunque per la prossima giornata!**

## [👉🏻 **Scarica la locandina**](https://gitea.it/openit-este/materiale-grafico/src/branch/master/locandine/coderdojo/Coderdojo.pdf)

---

**Organizzato da:**

![Sculdascia Lab](sculdascialab.webp)


**In collaborazione con:**

![OpenIT Este](/images/ilseste.webp)
