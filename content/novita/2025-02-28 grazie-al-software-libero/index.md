---
title: Grazie al Software Libero
description: Come è andata sabato 15 febbraio? Ve lo raccontiamo qui
date: 2025-02-28T11:30:00+02:00
authors:
    - loviuz
slug: grazie-al-software-libero-2025
images:
    - grazie-al-software-libero.webp
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - fsfe
    - ilovefs
---

## 📋 Inizio di giornata
Sabato 15 febbraio abbiamo organizzato la giornata di **[Amo il Software Libero](/novita/i-love-fs-2025)**, una giornata organizzata da **FSFE** ([Free Software Foundation Europe](https://fsfe.org)) per dire "Grazie" a chi sviluppa e contribuisce al Software Libero.

Al mattino eravamo in centro a Este, al mercato settimanale del sabato, per incontrare le persone di passaggio, lasciando a disponizione materiale informativo su cos'è il software libero, GNU/Linux e più in generale cos'è questa "oscura" tecnologia che si usa tutti i giorni e che ormai sembra quasi inseparabile. Argomento non facile da trattare in pochi minuti, ma contiamo di aver lasciato un po' il segno.

Pausa pranzo al bar **San Marco**, censito anche su [OpenStreetMap](https://osmapp.org/node/1713154182#17.00/45.2203/11.6797).


## 💌 Pomeriggio
Il pomeriggio è iniziata la presentazione con le persone presenti al Patronato Ss. Redentore, dove abbiamo riproposto un'introduzione piuttosto classica sull'argomento e su chi sono FSFE, promotrice della campagna, e Italian Linux Society, entrambe sostenitrici e promotrici di questa filosofia. Al di là della presentazione frontale, che serviva inizialmente a introdurre l'argomento, è stato interessante poi come la giornata abbia preso una piega più coinvolgente tra le persone partecipanti, con domande pratiche e discussioni sulla tecnologia in senso più ampio (IA, dati personali, privacy, ecc), anche in seguito a importanti cambiamenti oltreoceano, di aspetto politico, ma che volenti o nolenti riguardano la vita delle persone, e senza entrare nel merito sono talvolta molto pesanti su **trasparenza**, **riservatezza**, **sicurezza**, **democrazia** e **diritti umani**.

{{< img src="6ca85339464eac4f.jpg" title="Pomeriggio a I Love Free Software" alt="I Love Free Software a Este (PD)" author="Carlo Baratto" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}


Per mettere alla prova le persone partecipanti, ad un certo punto abbiamo giocato ad un quiz ospitato sulla piattaforma [Fuiz](https://fuiz.us/it/), una piattaforma libera per creare giochi e quiz a domande, con classifica. Dopo le ben 25 domande sulle conoscenze tecnologiche, molto stimolanti, la vittoria è andata a **Filippo** e **Raffaella**, che ringraziamo per la loro presenza e tenacia!

_In attesa che Fuiz pubblichi il quiz che abbiamo creato, qui si può scaricare un file da importare su Fuiz per usare le stesse domande:
[ilovefs.toml](ilovefs.toml)_

{{< img src="IMG_20250215_171525_675.jpg" title="Fabio Lovato con Filippo e Raffaella" alt="I Love Free Software a Este (PD)" author="Michele Bovo" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

Il premio che hanno ricevuto è stato il libro di Kenobit,**Liberare il mio smartphone per liberare  me stesso**, [scaricabile anche dal suo sito](https://www.kenobit.it/libri-e-fanzine/). Kenobit è un artista che fa musica con il gameboy e molte altre cose interessanti, e la scelta del suo libro è dovuta al fatto che anche lui divulga l'uso di software libero, social alternativi e promuove una forma di resistenza digitale. E' anche il gestore di [LivelloSegreto.it](https://livellosegreto.it), un'istanza di **Mastodon** su videogiochi, fumetti, musica, diritti, underground, controcultura, arte.


## ❤️ Grazie a queste persone

![FSFE_ILFS_sticker_50x90_2017.jpg](FSFE_ILFS_sticker_50x90_2017.jpg)

Abbiamo cercato di menzionare una serie di progetti di software libero per ringraziare chi vi ha contribuito e le persone che li mantengono, descrivendo anche a cosa servono per invitare altre persone a usarli ed eventualmente contribuire in tanti modi. Non siamo riusciti a presentarli tutti perché le discussioni con le persone presenti si sono ampliate su molti temi, per cui approfittiamo per scriverle qui.

Ringraziamo nuovamente anche **FSFE** per il materiale e il supporto, e per le linee guida per questa importante campagna.

### Hemmelig

**Presentato da:** Fabio Lovato

**Autore:** _bjarneo_ (Norvegia)

**Sito web:** https://hemmelig.app/

L'ho scoperto quasi per caso cercando dei servizi da caricare su _ServiziLiberi.it_. Permette di condividere in sicurezza note o password con link che si auto-distruggono. Personalmente lo uso a lavoro per condividere password in modo sicuro perché invio spesso per email a clienti o fornitori dei dati di accesso a piattaforme o software vari, specificando link di accesso, nome utente e, al posto della password, mando un link tramite Hemmelig che, dopo l'apertura, si auto-elimina. Un'istanza italiana è qui: https://notesicure.serviziliberi.it/


### Codice-fiscale (libreria PHP)

**Presentato da:** Fabio Lovato

**Autore:** _Davide Pastore_ (Italia)

**Sito web:** https://github.com/DavidePastore/codice-fiscale

Usiamo questa libreria a lavoro nel nostro gestionale _OpenSTAManager_ per verificare la correttezza del codice fiscale inserito nell'anagrafica cliente. Ha un funzionamento semplicissimo ed è molto valido. E' sviluppato da un italiano e credo sia usato solo in Italia da chi programma in PHP. Può anche generare un codice fiscale partendo da nome, cognome, data di nascita e gli altri parametri necessari.


### Super Productivity

**Presentato da:** Fabio Lovato

**Autore:** _Johannes Millan_ (Germania)

**Sito web:** https://super-productivity.com/

Applicazione per pc (ma anche telefono) che uso a lavoro per appuntare le cose da fare. La peculiarità è che permette di stimare il tempo disponibile giornaliero e quello di ogni singola attività, così da avere un calcolo di un carico di lavoro molto realistico di cose da fare. Ho contribuito a tradurre diversi testi mancanti in italiano e lo sto mantenendo tradotto tuttora perché lo uso quotidianamente.

### Meld

**Presentato da:** Fabio Lovato

**Autori:** _Kai Willadsen_ (Australia), _Stephen Kennedy_, _Vincent Legoll_

**Sito web:** https://gnome.pages.gitlab.gnome.org/meld/

Questo programma permette di confrontare tra loro file, evidenziare differenze e gestirne l'unione (merge). Di base, il confronto avviene su 2 file, oppure a 3 nel caso si lavori con un sistema di versionamento dei file, oppure si possono confrontare intere cartelle tra loro, che abbiano i contenuti organizzati in modo simile, così da confrontare quali file ci sono in più, in meno o che differenze hanno. Fa parte dell'ambiente desktop GNOME ma non credo sia molto conosciuto.


### NTFY

**Presentato da:** Fabio Lovato

**Autore:** _Philipp Heckel_ (Australia)

**Sito web:** https://ntfy.sh/

E' un'app open source, ma anche un'applicazione lato server, che permette di ricevere delle notifiche push sul telefono con semplici richieste HTTP. Si può integrare con delle proprie applicazioni per costruire un sistema di notifiche personalizzato. Si può usare la loro istanza su https://ntfy.sh o crearne una propria. Si può sfruttare anche per ricevere notifiche per esempio su un sistema di domotica o per i casi più disparati.


### PDF Doc Scan

**Presentato da:** Fabio Lovato

**Autori:** _Vladas Maksimkinas_ (Lituania), _Gregor Niehl_ (Germania)

**Sito web:** https://github.com/LittleTrickster/PDF-Doc-Scan

E' un'app per Android che permette di fotografare dei fogli, rileva automaticamente i 4 angoli e raddrizza poi l'immagine stirandola, in modo da arrivare a generare un file PDF in A4 in 3 semplici passi. Senza pubblicità, va dritto al punto. Molto comodo per archiviare documenti al volo in PDF. Usa un sistema di riconoscimento degli oggetti tramite le librerie OpenCV (Computer Vision).


### Navidrome

**Presentato da:** Andrea Sarego

**Autore:** _Deluan Quintão_ (Canada)

**Sito web:** https://www.navidrome.org

Navidrome è un software open source per lo streaming musicale che permette di ascoltare la propria collezione ovunque ci si trovi. È un'ottima alternativa a servizi come Spotify per chi desidera avere il controllo completo sulla propria musica, senza dipendere da piattaforme proprietarie. Personalmente, l'ho installato sul NAS di casa mia.


### Tempo

**Presentato da:** Andrea Sarego

**Autore:** _Antonio Cappiello_ (Italia)

**Sito web:** https://github.com/CappielloAntonio/tempo

_Tempo_ è un client open source per _Subsonic_, sviluppato nativamente per Android. E' un'app che uso per collegarmi a Navidrome e ascoltare la mia libreria musicale in streaming direttamente dal telefono.


### KeePassXC

**Presentato da:** Carlo Baratto

**Autori:** _Jonathan White_ (USA), _Janek Bevendorff_ (Germania)

**Sito web:** https://keepassxc.org/

Si tratta di un gestore di password molto robusto e funzionale che supporta l'integrazione con il browser per facilitare la compilazione dei campi. Il vantaggio è che serve ricordare la password di sblocco di KeePassXC per avere a disposizione tutte le nostre password. Il software è multipiattaforma, compresi i telefoni.

