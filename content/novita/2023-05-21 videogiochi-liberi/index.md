---
title: Videogiochi Liberi
description: Scopri con noi i Videogiochi Liberi per tutte le età!
date: 2023-05-21T13:11:00+02:00
authors:
    - loviuz
slug: videogiochi-liberi
images:
    - videogiochi-liberi2.webp
layout: news
fullcover: true
tags:
    - videogiochi
    - softwarelibero
    - tecnologie
    - linux
---


## Introduzione

{{< rawhtml >}}
<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://funkwhale.it/front/embed.html?&amp;type=track&amp;id=11530"></iframe>
{{< /rawhtml >}}

## 🎮 Perché un videogioco deve essere libero?

Il gioco dovrebbe essere un diritto per tutte le persone e non dovrebbe comportare la cessione di dati personali a favore delle aziende o il supporto a dittature.

I giochi possono essere **formativi** e aiutare a sviluppare determinate capacità come la memoria, l'attenzione, la percezione, e possono aiutare a prendere decisioni difficili in breve tempo, oltre che stimolare la collaborazione e il rispetto.

Non sempre tutto questo è presente nei videogiochi commerciali, che spesso esasperano questi aspetti, usano algoritmi chiusi di intrattenimento, quindi non è possibile verificare come funzionano, e soprattutto non è possibile contribuire a migliorarli poiché sono software chiusi, e tra gli scopi principali c'è anche il profitto.


## 🙌🏽 Le alternative libere

Esiste la necessità di creare alternative che siano accessibili a tuttɜ. **Minetest**, per esempio, è nato in seguito al prodotto commerciale [Minecraft](https://it.wikipedia.org/wiki/Minecraft) di Microsoft, con la differenza che **Minetest è un motore di gioco** che consente ai giocatori una personalizzazione ampia e una partecipazione attiva allo sviluppo grazie alla sua natura di software libero. Inoltre, Minetest è completamente gratuito e incentrato sulla solidarietà e condivisione.


## 📲 Come partecipare

L'evento si terrà da remoto, significa che potrai partecipare comodamente da casa collegandoti dal tuo computer all'indirizzo che invieremo poco prima dell'inizio. Consigliamo di attivare la webcam per vedersi tra partecipanti e interagire meglio, e di usare un computer per poter provare al momento Minetest o eventuali altri giochi proposti.

Conferma la tua presenza da qui:

{{< call-to-action title="✔️ Voglio partecipare" url="https://mobilizon.it/events/456231a8-aec2-43c6-97ba-fff1c974afa6" >}}

L'evento è organizzato in collaborazione con:

[![Etica Digitale](etica-digitale.png)](https://eticadigitale.org)

## 📩 Contattaci

Se vuoi sapere in generale dove e come trovarci, puoi [contattarci da qui](/partecipa/#come-rimanere-in-contatto)!


## 📹 Guarda la registrazione

{{< rawhtml >}}
<iframe title="Videogiochi Liberi" src="https://peertube.uno/videos/embed/5d27fd51-2708-4ff1-8498-6cf497c8e045" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="100%" height="500" frameborder="0"></iframe>
{{< /rawhtml >}}
