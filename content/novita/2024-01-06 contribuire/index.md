---
title: Iscriviti a ILS o fai una donazione
description: Contribuisci a sostienere le nostre attività associandoti o con una donazione
date: 2024-01-06T18:39:00+02:00
authors:
    - loviuz
slug: come-contribuire
images:
    - linux-161107_1280.webp
layout: news
fullcover: true
tags:
    - ils
    - donazioni
---

## Perché contribuire

Organizziamo incontri e aiutiamo le persone a capire come funzionano computer, telefoni e strumenti digitali (app o programmi), proponendo soluzioni alternative e libere. In particolare, **aiutiamo a installare GNU/Linux** (detto spesso solo "Linux") sul proprio computer fisso o portatile, per dare modo di usarlo con programmi sviluppati e mantenuti da una comunità mondiale di volontari e aziende, che rilasciano tutto questo con licenze libere, dando quindi trasparenza all'utilizzatore finale di cosa sta usando e permettendo di vederne il codice, modificarlo, copiarlo e ridistribuirlo, al contrario di sistemi operativi proprietari come Windows o MacOS.

Aiutiamo anche a capire come funzionano le app sul proprio telefono per **proporre strumenti alternativi rispettosi delle persone e della loro privacy**.

Per fare tutto questo, dedichiamo parte del nostro tempo libero per studiare queste alternative libere, imparare come funzionano, installarle, dare supporto, partecipare allo sviluppo e alle traduzioni, per avere programmi e app liberi sempre aggiornati e tradotti nella propria lingua, e talvolta anche traducendo i manuali d'uso.

E' in partenza, poi, il progetto **Servizi Liberi** che permetterà a tutte le persone di soddisfare necessità quotidiane (conversione di PDF, sondaggi, ricette, ecc) senza dipendere da programmi o app proprietari e chiusi, senza essere profilati e quindi senza alcun tipo di pubblicità mirata. Per fare in modo che tutto questo sia sostenibile ti chiediamo di contribuire in base alle tue necessità come proposto qui sotto. 👇

Qui trovi le nostre entrate e uscite aggiornate:

https://cryptpad.devol.it/sheet/#/2/sheet/view/-Yyv8kGnXkL6hRqmemgYh1E7aivrhC4r3-4hSdAX4K4/



## Come contribuire

### 🚀 Iscriviti a ILS

Puoi iscriverti a [ILS (Italian Linux Society)](https://www.ils.org/) tramite il modulo di registrazione, scegliendo di associarti alla sezione locale di Este, così che una parte della quota di iscrizione venga dedicata alla nostra sezione locale per iniziative ed eventi:

![Come associarsi a ILS Este](come-associarsi.webp)

La quota di iscrizione annuale è di **25 €** e si può pagare via Paypal o bonifico bancario. I soci hanno diritto a:
- un indirizzo di posta nome.cognome@linux.it ed uno nome.cognome@ils.org (o simile, in caso di omonimie)
- un indirizzo di posta nickname@linux.it ed uno nickname@ils.org (previa disponibilità del nome scelto)
- una mailing list nomeascelta@lists.linux.it
- accesso alla mailing list privata dei Soci
- aggregazione del proprio sito o blog su Planet ILS

{{< call-to-action title="Iscriviti a ILS Este ➡️" url="https://ilsmanager.linux.it/ng/register/to/este/11" >}}



### 🎁 Donazione libera

Se non vuoi associarti ma vuoi comunque sostenere le nostre iniziative o vuoi dare un contributo economico generico, puoi effettuare un bonifico con la causale **Donazione sezione Este** su Paypal o con bonifico bancario dell'importo che preferisci:
- accredito sul conto PayPal intestato a direttore@linux.it
- bonifico bancario sull'IBAN IT74G0200812609000100129899 intestato a "ILS Italian Linux Society"



Foto di [OpenClipart-Vectors](https://pixabay.com/it/users/openclipart-vectors-30363/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=161107) da [Pixabay](https://pixabay.com/it//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=161107) (modificata)
