---
title: "Software Libero a scuola, ma davvero"
description: "Sostenibilità e Sovranità Digitale nelle scuole del Veneto"
date: 2024-12-21T19:09:00+02:00
authors:
    - loviuz
slug: linux-a-scuola
images:
    - fuss-veneto.webp
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - scuola
    - regioneveneto
    - usr
---


L'**[Ufficio Scolastico Regionale (USR) del Veneto](https://istruzioneveneto.gov.it)**, in collaborazione con gli **Uffici Ambiti Territoriali (UU.AA.TT.)**, ha recentemente pubblicato quanto è stato studiato e proposto dal **Gruppo di lavoro regionale sull'Educazione alla Legalità**. Questo gruppo ha prodotto un documento dettagliato che illustra **come adottare il Software Libero nelle scuole**.

Il gruppo di lavoro, composto da circa cento docenti delle scuole della Regione Veneto, ha organizzato quattro incontri formativi e informativi da remoto grazie al coordinamento delle referenti di Legalità, Politiche Giovanili e Partecipazione degli Uffici Ambiti Territoriali di Verona e Vicenza.

All'interno di questo gruppo, il Sottogruppo 5 dedicato a **Piattaforme open-source** è stato costituito da quattro docenti provenienti da diversi ordini di scuola e territori, insieme a un ispettore tecnico:

 - **Gianluca Maestra**, docente di scuola secondaria di secondo grado di Padova
 - **Monica Marton**, docente e collaboratrice del dirigente scolastico presso l'Istituto Comprensivo 3 di Vicenza
 - **Eleonora Leta**, docente e collaboratrice del dirigente scolastico dell'Istituto Comprensivo 8 di Verona
 - **Nicola Vicentini**, docente presso l'Istituto Comprensivo 5 di Padova
 - **Paolo Dongilli**, Coordinatore del Progetto FUSS ed ispettore presso l'Intendenza Scolastica Italiana, Provincia Autonoma di Bolzano

Negli incontri, docenti, tecnici ed esperti hanno discusso il tema del Software Libero, le sue applicazioni, esempi di buone pratiche e l'analisi dell'**esperienza del progetto FUSS di Bolzano**, per valutarne la possibile replica in Veneto. Alla realizzazione di questo progetto hanno collaborato anche esperti esterni, come **Albano Battistella** attivo nel progetto **Zorin OS** e membro di **ILS Vicenza**, **Marco Marinello** dell'azienda Qnets, **Mauro Biasutti** dell'azienda **Continuity** e **Fabio Lovato** di **ILS Este** e **direttore ILS nazionale**.

Il materiale prodotto è disponibile sul portale dell'USR Veneto e sul sito del progetto FUSS: https://fuss.bz.it/fuss-veneto

Il progetto partirà da 3 scuole volontarie seguendo il percorso proposto. Interessante anche notare sotto l'aspetto economico l'enorme risparmio di soldi pubblici in licenze.

### Fonti
 - Articolo dal blog di FUSS: https://fuss.bz.it/post/2024-11-07_fuss-veneto
 - Progetto FUSS: https://fuss.bz.it/
 - LugBZ: https://lugbz.org/
 - ILS Vicenza: https://vicenza.ils.org/
