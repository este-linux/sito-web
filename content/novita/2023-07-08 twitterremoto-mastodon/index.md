---
title: 🎧 L'elefante e l'uccellino
description: Ennesimo terremoto di Elon Musk su Twitter. Le forti limitazioni per gli utenti della piattaforma hanno fatto infuriare, e parte un nuovo esodo verso Mastodon.
date: 2023-07-08T18:24:00+02:00
IgnoreTableOfContents: false
authors:
    - loviuz
slug: twitterremoto-mastodon
images:
    - twitterremoto-mastodon.webp
layout: news
fullcover: false
tags:
    - podcast
    - twitter
    - mastodon
    - socialnetwork
---

## Via al podcast!

Recentemente abbiamo deciso di intraprendere un viaggio per confrontarci, parlare di Linux e software libero tramite un podcast che troverete in questo spazio. Le chiacchierate amichevoli vogliono far incuriosire chi non conosce l'argomento; avvicinare e magari conoscere chi è interessato a far parte del gruppo, sia in presenza che nei nostri canali in rete, mitigando la paura iniziale che si ha verso Linux. Non siamo tutti esperti, ma la diversità di esperienze e conoscenze è la base per essere alla portata di tutti.

Di seguito un breve riassunto del primo esperimento. Nel podcast ci sarà una iniziale presentazione dei membri e del gruppo.

Buon ascolto!

{{< rawhtml >}}
<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://funkwhale.it/front/embed.html?&amp;type=track&amp;id=12667"></iframe>
{{< /rawhtml >}}


## Cos'è successo?

Luglio inizia con Twitter fortemente limitato nella visualizzazione dei contenuti, sia per utenti non paganti che per abbonati. La nuova scusa di Elon Musk è che Twitter sarebbe sfruttato pesantemente dalle **intelligenze artificial (IA)** che, tramite programmi automatizzati, eseguono il ["data scraping"](https://en.wikipedia.org/wiki/Data_scraping) proprio sulla sua piattaforma, ossia la raccolta di dati e informazioni in modi non convenzionali, per leggerne i contenuti e archiviarli per addestrare gli algoritmi di allenamento (training). Normalmente, piattaforme di questo tipo, mettono a disposizione delle **interfacce apposite (API)** per la lettura dei dati in modo strutturato, studiate appositamente per far dialogare due programmi tra loro, ad esempio per estrarre statistiche sui contenuti pubblicati, gli hashtag, gli utenti più attivi, ecc, ma dopo la forte limitazione anche di queste interfacce da parte di Elon scattata già mesi fa, probabilmente gli sviluppatori di IA si sono inventati di usare il data scraping, tecnica che può rallentare di molto una piattaforma durante, appunto, il cosiddetto "scraping", cioè il "rastrellamento" dei contenuti.


## Fuga verso Mastodon

La limitazione agli utenti di Twitte di Elon Musk ha fatto infuriare moltissimi utenti, tanto che sulla principale istanza Mastodon italiana, [Mastodon.uno](https://mastodon.uno), i server non hanno retto e per diverse ore è rimasta inaccessibile visto che vecchi utenti registrati mesi fa hanno ripreso a usare Mastodon, e in più altri nuovi utenti si sono iscritti per cercare un social network alternativo.

Mastodon si sta accollando il peso della rabbia degli utenti di Twitter, in tutti i sensi!

Fonte: https://www.ilpost.it/2023/07/03/twitter-musk-limitazione-numero-tweet/
