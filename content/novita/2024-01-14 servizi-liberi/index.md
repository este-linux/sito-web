---
title: Oggi nasce ServiziLiberi.it!
description: Utilizza i servizi su internet senza profilazione, senza pubblicità e in modo più rispettoso della tua privacy
date: 2024-01-14T15:00:00+02:00
authors:
    - loviuz
slug: servizi-liberi
images:
    - servizi-liberi.webp
layout: news
fullcover: false
tags:
    - ils
    - serviziliberi
IgnoreTableOfContents: true
---

**ServiziLiberi** é una soluzione che abbiamo deciso, da qualche mese, di rendere disponibile alle persone per poter usare una serie di strumenti digitali in modo libero.

Alla data di lancio contiene 2 applicazioni:
 - **Stirling PDF**, uno strumento potente e molto funzionale, per elaborare i file PDF da computer, telefono o altro dispositivo con un browser, e contiene ad oggi 53 strumenti diversi!
 - **Mealie**, un'applicazione per creare, importare e gestire le tue ricette di cucina. La peculiarità è quella di poter importare da vari siti web le ricette, recuperare l'immagine di copertina, gli ingredienti, separarli in liste, dividere i vari passaggi e visualizzare i valori nutrienti (proteine, grassi, zuccheri, ecc). Permette inoltre di gestire la lista della spesa.

Questo è solo l'inizio, a breve verranno resi disponibili anche altri servizi liberi.

## Cosa intendiamo con "servizi liberi"

Ci siamo ormai abituati che tutto ciò che troviamo in rete è gratuito, ma chiunque metta a disposizione delle soluzioni tecnologiche, programmi o app deve sostenere dei costi. La gratuità di ciò che usiamo, spesso viene utilizzata per attirare nuovi utenti, fargli provare un prodotto per poi vendergli eventualmente una versione più ricca e con altre funzioni, il che non è sbagliato, se non fosse che ciò che usiamo gratuitamente in realtà lo è solo a livello monetario, e quindi **paghiamo con i nostri dati**.

![Free candy](freecandy.jpg)

**Google**, **Meta** e molti altri colossi tecnologici, infatti, generano fatturati da capogiro ogni anno, si parla dell'ordine di centinaia di miliardi, e buona parte di questi introiti derivano dallo sfruttamento dei nostri dati, utilizzando ciò che inseriamo nelle attività di tutti i giorni, tra cui **Gmail**, **YouTube**, **Google Maps**, **Google Photo**, **Google Wallet**, **Instagram**, **Whatsapp**, **Facebook**, **X (ex-Twitter)** e molto altro. Nel solo 2022, Google ha realizzato ricavi per circa 282.000.000.000 dollari (fonte: https://www.analisicorporate.net/alphabet-google-risultati-trimestrali/). I costi di ricerca e sviluppo sono stati di circa 39.000.000.000 dollari. Di certo non è un cookie il loro ostacolo più grande, hanno un bel po' di risorse per sfruttare ogni nostro singolo dato.

![Accetta cookie e termini e condizioni d'uso](accetta.jpg)

Vogliamo essere ottimisti perché, da un po' di anni, sono nati gruppi che costruiscono alternative libere, come [Devol](https://devol.it/), un gruppo che si batte per la **degooglizzazione** di Internet, fornendo servizi sia gratuiti ma soprattutto liberi, per cui **utilizzando software libero, rispettando la privacy e non profilando o inviando a terzi i dati dei propri utenti**. Sono anche i gestori della più grande istanza Mastodon italiana, [mastodon.uno](https://mastodon.uno).

Abbiamo anticipato già tempo fa [come proteggere la propria privacy attraverso il software libero](/progetti/privacy-software-libero/), proponendo software o soluzioni rispettose dei dati degli utenti.

> Oggi abbiamo deciso di mettere a disposizione le nostre conoscenze e competenze informatiche per fornire dei servizi **liberi, alternativi, decentralizzati**!

Per iniziare a usare questi strumenti liberi, collegati al sito dal pulsante qui sotto:

{{< call-to-action title="Accedi a ServiziLiberi.it ➡️" url="https://serviziliberi.it" >}}

All'interno trovi anche come contribuire con una donazione poiché preferiamo sostenere questi servizi in euro piuttosto che raccogliendo i tuoi dati e con metodi poco trasparenti.

## 🙄 Non è chiaro?
Se non hai capito nulla ma ti sembra utile, interessante, o allarmante, non ti preoccupare. Abbiamo un gruppo Telegram chiamato **ILS Este - Gruppo** (ti puoi unire da https://t.me/openit_este se hai Telegram), e **ci troviamo periodicamente a Este**, trovi tutte le informazioni nella pagina [Partecipa](/partecipa/).