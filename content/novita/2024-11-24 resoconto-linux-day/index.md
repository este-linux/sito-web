---
title: Come è andato il Linux Day 2024?
description: La scuola come punto di partenza per trovare nuove leve per sviluppare il software libero
date: 2024-11-24T18:39:00+02:00
authors:
    - loviuz
slug: resoconto-linux-day-2024
images:
    - intro.jpg
layout: news
fullcover: true
IgnoreTableOfContents: false
tags:
    - linux
    - linuxday
    - iniziative
---


## Il Software Libero nella scuola

Il mattino abbiamo organizzato l'evento all'[IIS J.F. Kennedy di Monselice (PD)](https://www.iiskennedy.edu.it/) per parlare a studenti e studentesse di informatica, insieme ad altri **51 eventi in contemporanea in tutta Italia**.

Abbiamo raccontato come sono nati i computer, poiché la storia insegna sempre tantissimo, arrivando fino ai giorni nostri e avviando anche una breve discussione sui **dati**, il vero fulcro su cui ruota e ruoterà tutto in questo millennio. Abbiamo inoltre portato degli esempi pratici di software libero da usare tutti i giorni in alternativa a quelli più comuni. Con Linux e il software libero, infatti, torniamo a **riconquistare le nostre libertà digitali**.

E' importante che chi costruirà software o qualsiasi strumento digitale (anche hardware) sappia cos'è il software libero, soprattutto perché **le loro scelte influiranno sul mondo che viviamo** ma che probabilmente vivranno le generazioni future.

### Non sappiamo nulla

Sempre più azioni della nostra vita sono digitali anche se non ce ne rendiamo conto, anche la semplice lettura di questo articolo coinvolge una infinità di software più o meno liberi: il browser, il sistema operativo, magari l'antivirus che scansiona il traffico, oppure un'applicazione o un'app che può interagire con il browser, l'ubicazione stessa dei dati di navigazione se sincronizziamo la cronologia del browser con qualche servizio più o meno libero. Avvengono un'infinità di micro-operazioni di cui sappiamo poco o nulla, per cui **siamo costretti a fidarci**, e spesso dove non c'è regolamentazione o non ci sono espressi divieti, le aziende o terzi trovano modo di sfruttarle (profitto, spionaggio, qualsiasi altra cosa), passando da queste nostre micro-azioni e comportamenti per noi apparentemente innocui.

Non serve immaginare a come potranno essere usati questi **dati** in futuro, perché abbiamo già avuto prova di come lo sono stati. Guardiamo per esempio la cosiddetta **intelligenza artificiale**: molti dati, spesso presi senza chiedere scansionando la rete internet, hanno permesso di creare soluzioni incredibilmente innovative e fenomenali, ma nelle mani di pochi grazie alle nostre "tracce" lasciate generosamente in rete. Etico, no?
Pensiamo anche solo agli **algoritmi di profilazione** applicati alla **pubblicità mirata**, o ai contenuti social: **siamo noi a ricercare le notizie?** Oppure ci viene proposto che cosa leggere in modo così automatico, facile e veloce, tale per cui non sappiamo neanche più dove e come cercarle? E se questi strumenti cadessero nelle mani sbagliate? Non serve tanta immaginazione, è già successo con la Brexit e le elezioni in USA nel 2016, vedi lo scandalo Cambridge Analytica e molti altri ancora.

> Il software è il cuore delle nostre vite ormai quasi interamente digitali, ma le prossime leve che costruiranno software, come vorranno cambiare il mondo?

Fortunatamente, almeno in Europa, arrivano norme come il GDPR per limitare questo potere, ma non basta. E' compito della **società civile** intervenire in tempo, per cui **è importante divulgare come funzionano le tecnologie**, spiegare perché occorre usare software libero, il quale ne riduce se non addirittura annulla certi effetti negativi e rischi, e soprattutto per **chi progetta e sviluppa software** è importante continuare a portare avanti questa filosofia e modo di sviluppare il software. Chi meglio di chi sta studiando informatica? Ecco perché negli ultimi anni abbiamo scelto proprio la scuola come terreno per divulgare questa mentalità.

> **Ringraziamo** la professoressa Anna Maria Baccan con la quale ci siamo interfacciati per organizzare l'evento e inoltre tutto il corpo docente che ha portato le classi e permesso la riuscita dell'evento nella mattinata.


## Pomeriggio libero

Non è stato "libero" da impegni, ma libero da un programma preciso e senza presentazioni formali, per accogliere persone curiose, discutere e sperimentare. C'è stato modo di conoscere persone nuove e di condividere esperienze. Grazie al Linux Day, uno studente ci ha conosciuti e ha partecipato al successivo [corso su Docker](/novita/docker-2024)!


## Materiale

Di seguito le presentazioni utilizzate nella mattinata:

| Autore | Titolo | Presentazione |
|--------|--------|---------------|
| Fabio Lovato | Dati, profilazione e alternative libere | **https://este.linux.it/varie/presentazioni/2024-10-26%20LD2024/** (file sorgenti: https://codeberg.org/este-linux/presentazioni/src/branch/main/2024-10-26%20LD2024)
| Carlo Baratto | Software libero: dalle origini a oggi | [Storia2.odp](Storia2.odp) |
| Michele Bovo | Millemila cose da fare con il software libero | [Millemilacose.odp](Millemilacose.odp) |


## Foto dell'evento

{{< img src="5830449754390250732.jpg" title="Apertura dell'evento" alt="Linux Day a Este (PD)" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="5830036892068989390.jpg" title="Fabio Lovato apre l'evento con la mappa dei Linux Day nel 2024 in Italia" alt="Linux Day a Este (PD)" author="Michele Bovo" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="5830036892068989387.jpg" title="Carlo Baratto mentre spiega l'evento che diede origine al progetto GNU" alt="Linux Day a Este (PD)" author="Michele Bovo" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="5830036892068989388.jpg" title="Carlo Baratto spiega l'era dell'informazione oggi" alt="Linux Day a Este (PD)" author="Michele Bovo" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="5830036892068989389.jpg" title="Michele Bovo mentre spiega mille cose da fare con il software libero" alt="Linux Day a Este (PD)" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="5830036892068989391.jpg" alt="Fabio Lovato spiega cosa sono i dati oggi" author="Michele Bovo" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="5830036892068989392.jpg" title="Schermata di Claper durante un test di interazione con le persone partecipanti" alt="Linux Day a Este (PD)" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}

{{< img src="DSC_0030.jpg" title="Michele Bovo a sinistra e Andrea Sarego a destra mentre modera i contenuti" alt="Linux Day a Este (PD)" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}
