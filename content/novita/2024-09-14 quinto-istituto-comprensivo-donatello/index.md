---
title: Volontariato e Innovazione
description: Un Nuovo Inizio per l'Aula di Informatica del 5° Istituto Comprensivo "Donatello" di Padova"
date: 2024-09-14T15:12:00+02:00
authors:
    - loviuz
slug: quinto-istituto-comprensivo-padova
images:
    - linux-a-scuola-quinto-istituto-comprensivo-donatello-padova.webp
layout: news
fullcover: true
tags:
    - ils
    - attività
    - padova
    - veneto
    - scuola
    - zorin
IgnoreTableOfContents: true
---

Recentemente, noi di ILS Este abbiamo avuto l'opportunità di contribuire a un'iniziativa significativa grazie alla collaborazione con [PN Lug](https://www.pnlug.it/), Pordenone Linux User Group, un'associazione di promozione sociale che opera nel territorio con le persone e le scuole per la diffusione della cultura del software libero. Il nostro obiettivo era quello di sostituire, nell'aula di informatica del 5° Istituto Comprensivo "Donatello" di Padova, i computer che ormai erano diventati vecchi e obsoleti.

{{< img src="DSC_0001.jpg" alt="Volontari di ILS Este, PN Lug e corpo docente scuola" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}


Grazie al recupero di PC usati dal PN Lug, che altrimenti sarebbero finiti in discarica, siamo riusciti a dotare l'aula di dispositivi funzionanti. Sappiamo, purtroppo, che i **sistemi operativi proprietari**, grazie all' obsolescenza programmata, non permettono di aggiornarsi alla versione successiva, per cui si è costretti ad acquistare sempre nuovi computer, il che non è sostenibile per l'ambiente e non va nella direzione dell'[Agenda 2030](https://it.wikipedia.org/wiki/Obiettivi_di_sviluppo_sostenibile), e obbliga soprattutto le scuole a sostenere costi non necessari, togliendoli da altri capitoli o rinunciando ad attrezzatura informatica efficiente. Per evitare questi problemi, è stato installato **Zorin OS**, un sistema operativo basato su Linux e software libero, il quale offre numerosi vantaggi. Tra questi: la **libertà di utilizzo**, la possibilità di **personalizzazione** e l'**assenza di costi di licenza**. Inoltre, Zorin OS è progettato per essere leggero e performante, permettendo ai computer più datati di funzionare in modo efficiente per molti anni. Ha inoltre delle interfacce utente semplici e intuitive, e nell'occasione sono stati installati dei programmi per la dislessia, il daltonismo e altre problematiche, e altri molto utili per l'ambito scolastico.

{{< img src="DSC_0003.jpg" alt="Volontari di ILS Este, PN Lug e corpo docente scuola" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}



## L'importanza del software libero

Il riuso di PC datati, unito all'adozione di software libero, presenta vantaggi significativi per le scuole. In primo luogo, consente di ridurre i costi per l'acquisto di nuove attrezzature, permettendo di investire in altre aree educative. Inoltre, l'uso di software libero e open source promuove l'insegnamento di competenze digitali fondamentali, incoraggiando studenti e studentesse a esplorare e comprendere il funzionamento dei sistemi informatici.

È importante ricordare che le scuole dovrebbero rispettare il Codice dell'Amministrazione Digitale (CAD), nello specifico gli articoli 68 e 69, i quali promuovono direttamente l'uso di software libero nelle pubbliche amministrazioni poiché rendono obbligatoria l'**analisi comparativa scritta**, secondo criteri ben specifici definiti da AGID, prima di poter acquisire software o servizi digitali. In questa analisi comparativa viene data, **sempre**, precedenza al software open source, grazie alla quale le scuole possono garantire una maggiore protezione della privacy di studenti e studentesse, poiché hanno il controllo completo sui software installati e di conseguenza sui dati e sulle informazioni trattate.

E' stato, infine, installato un sistema che supporta nell'insegnamento in ambienti di apprendimento digitale, eseguendo corsi di formazione virtuale o dando supporto remoto, il tutto dalla rete locale e senza utilizzare servizi a pagamento esterni.


## Ringraziamenti

L'iniziativa di volontariato è stata frutto di un lavoro di coordinamento fra **PN Lug** che ha recuperato i PC donati, volontari di **ILS Este** che, con il **personale scolastico** dell'istituto e altri volontari, hanno installato il sistema operativo e i programmi necessari per il laboratorio, grazie anche ad una **dirigente scolastica** illuminata, la quale ha sostenuto con forza, accettato e permesso l'iniziativa, segno di lungimiranza e attenzione a valori fondamentali come la sostenibilità, la condivisione, la libertà di accesso alla tecnologia e la protezione dei dati. Un passo importante verso un'educazione digitale più inclusiva e consapevole.

Se fate parte di una scuola e volete contattarci per qualsiasi approfondimento:

- **ILS Este:** estelinux (@) mailbox.org
- **PN Lug:** info (@) pnlug.it

{{< img src="DSC_0007.jpg" alt="Volontari di ILS Este, PN Lug e corpo docente scuola" author="Fabio Lovato" license-text="CC BY 4.0" license-url="https://creativecommons.org/licenses/by/4.0/deed.it" >}}


### Collegamenti utili:
- 5° Istituto Comprensivo "Donatello": https://www.quintoicpadova.edu.it/
- Zorin OS: https://zorin.com/
- ILS Este: https://este.linux.it
- PN Lug: https://www.pnlug.it/
- ServiziLiberi: https://serviziliberi.it/