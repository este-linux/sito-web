---
title: Linux Day 2023
description: Scopri cos'è Linux per abbandonare Windows e Mac!
date: 2023-10-02T14:30:00+02:00
authors:
    - loviuz
slug: linux-day-2023
images:
    - linuxday_fullcolor.jpg
layout: news
fullcover: true
IgnoreTableOfContents: true
tags:
    - linux
    - linuxday
    - iniziative
---

**Sabato 28 ottobre** 2023 torna il [Linux Day](https://www.linuxday.it/), l'evento italiano per promuovere **Linux** e il **software libero**.

Quest'anno riproponiamo l'evento il **mattino** all'[IIS Euganeo di Este](https://www.iiseuganeo.cloud/) con argomenti specifici per le classi di informatica. Questi studenti, infatti, sono coloro che potranno decidere il futuro del digitale: saremo controllati da intelligenze artificali chiuse, nelle mani di poche multinazionali, oppure di algoritmi aperti, sistemi decentralizzati e nelle mani delle persone?

Il **pomeriggio**, invece, proseguiremo l'evento al Patronato SS. Redentore in viale Fiume, 65, sempre a Este, con temi più pratici e diretti. Saremo a disposizione per far provare GNU/Linux, installarlo e proporre soluzioni libere.

## Mattino

Programma specifico per le classi di informatica dell'**IIS Euganeo** di Este:

> Attenzione: il programma può subire variazioni nei prossimi giorni

| Orario            | Argomento                                                                   | Relatore           |
|:-----------------:|-----------------------------------------------------------------------------|--------------------|
| **8:00 -  8:35**  | Linux e software libero: apriamo Internet                                   | Fabio (Loviuz)     |
| **8:35 -  9:10**  | 40 anni di storia dell'informatica in 30 minuti                             | Carlo (Kaneda)     |
| **9:10 -  9:45**  | Software libero per una scuola libera                                       | Albano Battistella |
| **9:45 -  10:00** | Domande e confronti                                                         | -                  |
| **10:00 - 10:10** | ⏰ Intervallo                                                               | -                  |
| **10:10 - 10:40** | Linux e software libero: riprendiamoci i dati                               | Fabio (Loviuz)     |
| **10:40 - 11:15** | I nostri dati sono al sicuro in Europa? Il GDPR                             | Andrea Bordin      |
| **11:15 - 11:50** | Fairphone e telefoni liberi: filosofia del riuso e dell'uso software libero | Carlo (Kaneda)     |
| **11:50 - 12:00** | Domande, confronti e chiusura evento                                        | -                  |
| **12:00 - 13:00** | Spazio per approfondimenti o dimostrazioni con studenti e scuola            | -                  |

## Pomeriggio

Aperto a chiunque, liberamente e gratuitamente, al **Patronato SS. Redentore di Este in viale Fiume, 65**:

| Orario            | Argomento                                                 |
|:-----------------:|-----------------------------------------------------------|
| **14:30 - 18:30** | Laboratorio, installazione di Linux, tempo libero per sperimentare |

Bar aperto 🍻🥪


## Partecipa

Per gestire al meglio le sale consigliamo di indicare la partecipazione da Mobilizon:

https://mobilizon.it/events/5e5feaa5-a7b2-47bb-81a2-b9459574dddf

Non serve iscriversi, si può confermare anche solo inserendo il proprio nome e email per conferma, non verranno inviate pubblicità o usate in modo improprio.
