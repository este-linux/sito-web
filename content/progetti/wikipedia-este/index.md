---
title: Aggiornamento Este su Wikipedia
description: Mappatura del territorio e aggiornamento dati culturali di Este su Wikipedia
date: 2022-06-05T10:30:00+02:00
authors:
    - loviuz
images:
    - wikipedia-gdcbdb40d8_1280.webp
obiettivo_percent: 30
categories:
    - Cultura
tags:
    - wikipedia
    - cultura
    - este
    - italia
# status: new, wip, done
status: new
include_donations: true
---

Dall'app [OsmAnd~](https://osmand.net/), la prima app ufficiale di OpenStreetMap, mi sono accorto che abilitando alcuni plugin è possibile vedere articoli di **Wikipedia** che sono legati al luogo che si sta visitando, e ho scoperto che alcuni di questi luoghi non sono aggiornati o per assurdo sono stati scritti da utenti non italiani e sono in inglese, come ad esempio la Basilica di Santa Maria delle Grazie di Este (PD):

https://en.wikipedia.org/wiki/Santa_Maria_delle_Grazie,_Este

Ho così provveduto a tradurla, ecco la versione italiana:

https://it.wikipedia.org/wiki/Santa_Maria_delle_Grazie_(Este)

---

Ho descritto meglio come tradurre Wikipedia in un articolo:

[➡️ COME CONTRIBUIRE A WIKIPEDIA](/novita/come-contribuire-a-wikipedia)

---

**TO-DO**
- [X] tradurre la prima pagina (per dare l'esempio 🙂)
- [ ] controllare cosa è presente su Wikipedia riguardo Este
- [ ] aggiornare i contenuti
- [ ] crearne di nuovi
- [ ] proporre alle scuole la creazione di contenuti
