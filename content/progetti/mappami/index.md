---
title: Sorridi, sei su OpenStreetMap!
description: Entra a far parte della comunità globale dei mappatori liberi 
date: 2022-12-24T12:33:00+02:00
authors:
    - loviuz
images:
    - mappami.webp
obiettivo_percent: 50
categories:
    - Mappe
tags:
    - openstreetmap
    - contributori
    - mappami
# status: new, wip, done
status: wip
include_donations: true
---


Siete arrivati qui navigando il sito oppure ricevendo uno dei nostri biglietti 🙃


# Mappami

E' una campagna di sensibilizzazione che ha lo scopo di far conoscere [OpenStreetMap](https://it.wikipedia.org/wiki/OpenStreetMap), da qui abbreviato in OSM, un **progetto collaborativo** finalizzato a creare mappe del mondo a contenuto libero. Il progetto punta ad una raccolta mondiale di dati geografici, con scopo principale la creazione di mappe e cartografie. E' la principale alternativa a **Google Maps**.

![Anteprima front](preview1.webp)
![Anteprima back](preview2.webp)

{{< quick_summary >}}
- OpenStreetMap è un progetto collaborativo, quindi tutti possono contribuire e migliorare la mappa
- è gratuito e open source, il che significa che può essere utilizzato e modificato da chiunque
- fornisce dati di alta qualità che possono essere utilizzati in diversi modi, ad esempio per l'analisi geografica, la creazione di mappe personalizzate e per il proprio sito web
- offre una maggiore privacy rispetto a Google Maps poiché non raccoglie i dati di localizzazione dell'utente in modo aggressivo
- puoi aggiungere o modificare le informazioni sulla mappa utilizzando il sito web o diverse app per dispositivi mobili
- puoi unirti a un gruppo locale di mappatori e partecipare a eventi di mappatura in loco
- puoi utilizzare i dati di OpenStreetMap per creare mappe o applicazioni che possono essere condivise con gli altri
- usare e contribuire a OpenStreetMap aiuta a ridurre il monopolio di Google sulle mappe ma soprattutto a molti altri servizi
{{< /quick_summary >}}


## Perché non dovrei più usare Google Maps?
![Indipendenza Digitale](indipendenza-digitale.webp)

La tecnologia è presente in quasi tutti i momenti della nostra vita. Ci sono tecnologie che se non si conoscono entrano nella nostra vita involontariamente, o meglio, per volontà di chi le ha create. Google è una delle grandi aziende tecnologiche che sta monopolizzando Internet e i nostri dispositivi, per questo vogliamo promuovere alternative valide che per alcuni aspetti hanno anche vantaggi maggiori di Google. In particolare Google Maps fornisce mappe che rimangono di loro proprietà, vengono aggiornate solo da loro, ci sono [casi di censura](https://officinebrand.it/offpost/10-luoghi-oscurati-da-google-maps-uno-e-in-italia-in-provincia-di-torino/), non vi è possibile contribuire e soprattutto il loro utilizzo contribuisce a fornire a Google altri nostri dati, questa volta geografici.


## Privacy? Non mi interessa, non ho nulla da nascondere
![Privacy](privacy.webp)

Abbiamo già sentito questa affermazione. Farti capire che la privacy è importante e che è possibile ottenerla anche nel mondo digitale è una lunga questione ma puoi iniziare da questo breve percorso: https://eticadigitale.org/privasi/


## Come funziona OpenStreetMap
![OpenStreetMap](openstreetmap.webp)

Il sito principale è [OpenStreetMap.org](https://openstreetmap.org). Qui risiedono le mappe di tutto il mondo in un formato aperto e libero, per cui le puoi scaricare, modificare, aggiornare.

Il progetto segue la [filosofia del software libero](https://loviuz.me/news/software-libero-open-source-spiegone/), un po' come **Wikipedia**, quindi tutti i dati inseriti dagli utenti rimangono disponibili con una **licenza libera**, anche a scopo commerciale, sempre modificabili, con il solo requisito di citare la fonte.


## Cosa puoi fare
![Cosa puoi fare](cosa-puoi-fare.webp)

Se hai un negozio puoi aggiungere il tuo numero di telefono, l'email, puoi inserire un sacco di informazioni pubbliche e vederle online dopo pochi secondi! Se invece vuoi correggere una strada perché è diventata a senso unico, oppure vuoi aggiungere o aggiornare gli orari di un museo o locale lo puoi fare dal sito oppure da app dedicate che inviano sempre a OpenStreetMap ma sono pensate per facilitare l'inserimento di certi tipi di dati e sono più o meno semplici da usare, in base alle tue conoscenze informatiche. Si possono anche disegnare strade, fiumi, ponti, edifici e molto altro!



## Ma chi ci guadagna?

Tutti, ma in privacy e sicurezza dei propri dati!

![OSM e chi ci guadagna](osm-soldi.webp)



## Partiamo!
![Contribuisci a OpenStreetMap](corri.gif)

Puoi iniziare semplicemente rimuovendo o lasciando in disparte Google Maps. Poi, in base se stai usando uno smartphone o un pc, conviene scegliere delle app o siti web tramite cui usare o contribuire a OSM.



### Come usare OpenStreetMap
Puoi consultare le mappe in questo modo:
 - Da smartphone:
   - [OsmAnd](https://play.google.com/store/apps/details?id=net.osmand): app completa con molti plugin e funzionalità, tra cui un ottimo navigatore. Le mappe vengono scaricate nel telefono così da funzionare anche senza connessione
   - [OrganicMaps](https://play.google.com/store/apps/details?id=app.organicmaps&hl=it): mappa carina con vista 3D simulata e modalità di navigazione semplificata
   - [Qwant Maps](https://qwant.com/maps): sito web installabile come app o come segnalibro
 - Da computer:
   - [Sito ufficiale di OSM](https://openstreetmap.org): sito web ufficiale del progetto
   - [Qwant Maps](https://qwant.com/maps): sito web di un'azienda francese che ridistribusce le mappe sul loro sito e contiene anche altri servizi, come un motore di ricerca che non profila e non traccia
   - [OSMApp](https://osmapp.org/): alternativa carina graficamente per le mappe di OSM



### Come contribuire a OpenStreetMap
Per assurdo ci sono più siti e app per contribuire a OSM rispetto a quelle per consultarlo, ma vi proponiamo quelle che conosciamo e che crediamo siano più diffuse.

 - Da smartphone:
   - [StreetComplete](https://play.google.com/store/apps/details?id=de.westnordost.streetcomplete): prima app suggerita che permette di mappare dati incompleti in una modalità giocosa, nel senso che ad ogni mappatura si accumulano punti e si sbloccano altre sfide. Consigliatissima per iniziare!
   - [Every Door](https://play.google.com/store/apps/details?id=info.zverev.ilya.every_door): app abbastanza recente per mappare luoghi, strade e ambiente con una predisposizione migliore dei campi di inserimento
   - [OsmAnd](https://play.google.com/store/apps/details?id=net.osmand): app completa con molti plugin e funzionalità. Non offre moltissimi aiuti nel mappare
   - [Vespucci](https://play.google.com/store/apps/details?id=de.blau.android): per i più temerari che vogliono mappare in modo più spinto
 - Da computer:
   - [Sito ufficiale di OSM](https://openstreetmap.org): sito web ufficiale del progetto, editor molto semplice e intuitivo
   - [MapRoulette](https://maproulette.org/): sfide create dagli stessi utenti per correggere errori o migliorare la precisione dei dati
   - [MapComplete](https://mapcomplete.osm.be/): lista di tipologie da mappare, in cui ciascuna ha un'impostazione a domande, guidata, per mappare al meglio dei punti di interesse. Le tipologie stesse da mappare e i passaggi guidati sono sempre creati dagli utenti contributori


## Centra solo Google Maps?

No, è solo una piccola parte, ma in qualche modo bisogna pure iniziare, no? 😉 

## Come contribuire in 2 minuti

Cerca la tua attività su [openstreetmap.org](https://openstreetmap.org) (probabilmente l'abbiamo appena inserita noi). Se non c'è la puoi segnalare, in 2 minuti e senza bisogno di registrarti, tramite [su.openstreetmap.it](https://su.openstreetmap.it).


## Non sei interessato a questo argomento?

Non c'è problema, puoi rimanere in contatto con noi per altre iniziative o per approfondire riguardo temi su privacy, alternative e libertà digitali tramite il canale o il gruppo Telegram:
 - [Canale OpenIT Este - News](https://t.me/openit_este_news): per leggere notizie
 - [Gruppo OpenIT Este](https://t.me/openit_este): per scrivere e discutere


## Approfondimenti

Negli scorsi anni, e tuttora, Google è protagonista in diversi casi di violazione dei dati personali:
- Google ha raccolto e archiviato le posizioni degli utenti delle sue app per anni, senza chiedere il permesso degli utenti o informarli della raccolta dei dati - The Guardian (https://www.theguardian.com/technology/2018/aug/13/google-location-tracking-android-iphone-mobile)
 - Google ha utilizzato il suo sistema operativo per smartphone Android per **raccogliere dati di geolocalizzazione** e altre informazioni personali degli utenti, anche quando le app erano utilizzate in modalità "privata" - Tech HindustanTimes (https://tech.hindustantimes.com/tech/news/google-is-tracking-your-movements-like-it-or-not-story-gbZSPDBWZ37VbiARifJzaL.html)
 - Nel 2010, Google ha ammesso che Street View aveva accidentalmente raccolto informazioni personali, come **indirizzi e-mail e password**, dalle reti Wi-Fi pubbliche durante il rilevamento delle immagini delle strade - BBC News (http://news.bbc.co.uk/1/hi/8684110.stm)
 - Nel 2010, Google ha lanciato Google Buzz, un servizio di microblogging integrato con Gmail. Tuttavia, il servizio ha automaticamente reso pubblici gli **indirizzi e-mail degli utenti**, mettendo a rischio la loro privacy - ZDNet (https://www.zdnet.com/article/internet-buzzing-about-googles-privacy-blunder-with-buzz/)
 - Nel 2018, Google ha ammesso di aver nascosto una vulnerabilità di sicurezza in Google+, il suo servizio di social networking, che ha permesso a sviluppatori di app di **accedere ai dati personali degli utenti senza il loro consenso**. A causa di questa violazione della privacy, Google ha deciso di chiudere il servizio nel 2019 - CBS News (https://www.cbsnews.com/video/google-shutting-down-after-data-exposed/)