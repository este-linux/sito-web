---
title: Where Are You?
description: La geolocalizzazione personale privata e libera
date: 2025-01-05T10:30:00+02:00
authors:
    - kaneda
images:
    - where-are-you.webp
obiettivo_percent: 60
fullcover: true
categories:
    - Privacy
tags:
    - software libero
    - alternative
    - privacy
# status: new, wip, done
status: new
include_donations: true
---

# WhereAreYou
Ognuno di noi ha già utilizzato applicazioni come Google Maps o WhatsApp per condividere la propria posizione con amici e familiari. Tuttavia, non tutte queste app sono progettate per una condivisione rispettosa dei dati e della privacy.
WhereAreYou è un software gratuito che permette di condividere la propria posizione con gli altri senza dover pagare un canone e mantiene i dati nel proprio computer grazie al self hosting senza database.


![Map](https://blog.carlobaratto.it/wp-content/uploads/2025/01/SC1-461x1024.jpg)

![Settings](https://blog.carlobaratto.it/wp-content/uploads/2024/12/Screenshot_20241229_230914-461x1024.png)

![Menu](https://blog.carlobaratto.it/wp-content/uploads/2024/12/Schermata-del-2024-12-29-23-08-08.png)

## Caratteristiche principali
**Condivisione della posizione:**
è possibile condividere la propria posizione con amici e familiari direttamente dal telefono. È possibile scegliere se condividere la posizione in un determinato momento o in modo permanente.

**Pannello di controllo:**
È possibile visualizzare il pannello di controllo per controllare la propria posizione e attivare/disattivare la condivisione.

**Vantaggi:**
- **Sicurezza:** WhereAreYou si preoccupa della vostra sicurezza utilizzando tecnologie di crittografia avanzate per proteggere le vostre informazioni personali.
- **Risparmio:** Non dovrete pagare un canone mensile o annuale per utilizzare la nostra applicazione.
- **Facilità d'uso:** l'applicazione è facile da usare e richiede solo pochi passaggi per iniziare a condividere la propria posizione.

**Caratteristiche avanzate:**
- **3 indicatori visuali:**

Verde: utente visto meno di 10 minuti fa,

Arancione: utente visto tra 11 e 60 minuti fa,

Grigio: utente visto più di 60 minuti fa,

Dopo 120 minuti l'indicatore scompare.

- **Self-hosting:**
WhereAreYou è un software self-hosted, il che significa che potete ospitare i dati sul vostro computer. Questo vi garantisce il controllo completo dei vostri dati e impedisce la raccolta di dati personali da parte di terzi.
- **Nessuna registrazione della tua mail**
non è necessario registrare un account per utilizzare WhereAreYou. È sufficiente scaricare l'app e configurare l'indirizzo API per iniziare a condividere la propria posizione con gli amici.

## Come funziona
1. Scaricare e installare **WhereAreYou** sul telefono.
2. Dalla directory `/lib/PHPAPI/`, rinominare `config.sample.php` in `config.inc.php`, facendo attenzione a modificare `$user_api_key` e `$admin_api_key`. La chiave API utente deve essere condivisa SOLO con le persone con cui si vuole condividere la posizione, la chiave amministratore serve solo a chi gestisce il server.
3. Caricare entrambi sul proprio server.
4. Configurare l'app sul cellulare inserendo il proprio nickname, l'URL API e la chiave API utente impostata. La chiave API amministratore è facoltativa.
5. Iniziare a condividere la propria posizione con chiunque abbia lo stesso URL API e la stessa chiave API utente.
6. Utilizzare il pannello di controllo per controllare la propria posizione e attivare/disattivare la condivisione.

WhereAreYou è un software libero, gratuito e sicuro che consente di condividere la propria posizione con amici e familiari in tempo reale. Non è necessario pagare un canone mensile o annuale per utilizzare l'applicazione ed è facile da usare: bastano pochi passaggi per iniziare a condividere la propria posizione.

## Sviluppo

WhereAreYou nasce da una necessità di [Kaneda (Carlo Baratto)](https://github.com/carlobaratto/), che ne cura lo sviluppo con il prezioso aiuto di [Lorenzo Caraffini](https://github.com/LorenzoCaraffini) e del team di ILS Este.
I QA test sono svolti dal gruppo **[ILS Este Group](https://este.linux.it/)**.

Per contribuire o partecipare al progetto, scrivere a whereareyou@carlobaratto.it


## Da fare
- Rendere la grandezza dei markers dinamica
- Aggiungere il tempo di refresh personalizzato
- Criptare i dati lato server
