---
title: Privacy con il Software Libero
description: Riprenditi la tua privacy e passa al Software Libero!
date: 2023-04-13T22:57:00+02:00
authors:
    - loviuz
images:
    - privacy-software-libero.webp
obiettivo_percent: 10
categories:
    - Privacy
tags:
    - software libero
    - alternative
    - privacy
# status: new, wip, done
status: wip
include_donations: true
---


# Privacy con il Software Libero

Negli ultimi anni, l'attenzione sulla privacy online e sulla centralizzazione delle piattaforme di social media si è intensificata. Molti utenti stanno cercando alternative ai servizi tradizionali come Facebook, Twitter e Instagram, cercando soluzioni più libere e decentralizzate.

Una guida incredibilmente completa delle alternative è scritta e aggiornata su [LeAlternative.net](https://www.lealternative.net/).


{{< quick_summary >}}
- smetti, o almeno riduci drasticamente, il tempo passato sui social network tradizionali per ridurre la dipendenza
- smetti anche per uscire dalla bolla della sovra-informazione e inizia a informarti scegliendo TU cosa leggere e non lasciarti suggerire sempre dagli algoritmi
- usa solo software libero e riprenditi il controllo del tuo dispositivo
- scegli dei servizi cloud alternativi, liberi e rispettosi della privacy, e possibilmente locali o europei, per godere di una maggiore privacy
{{< /quick_summary >}}

Cambia social network e passa a Mastodon:
- è libero e dà maggiore trasparenza e libertà ai propri utenti
- non commercia i tuoi dati, a differenza dei social network tradizionali

Passa a Linux:
- è un sistema operativo libero che offre una maggiore trasparenza e libertà rispetto ai sistemi operativi proprietari come Windows e MacOS
- offre una maggiore sicurezza e privacy
- permette ai propri utenti di personalizzare il sistema secondo le proprie esigenze e preferenze, senza dover subire le restrizioni imposte dai produttori

Usa software libero:
- offre una maggiore trasparenza e libertà rispetto al software proprietario, permettendo ai propri utenti di personalizzarlo e condividerlo liberamente senza restrizioni
- è generalmente più sicuro e privato, grazie alla maggior attenzione alla qualità e alla protezione dei dati personali
- promuove la collaborazione e la condivisione tra gli utenti, permettendo la creazione di comunità e l'innovazione tecnologica

Evita Google, Facebook, Instagram, Twitter e gli altri social tradizionali:
- raccolgono e analizzano una quantità enorme di dati personali dei propri utenti, senza sempre rispettare la privacy e la trasparenza necessarie
- hanno posizione di monopolio nel rispettivo settore e della pubblicità digitale, limitando la concorrenza e l'innovazione
- promuovono una cultura di consumismo e manipolazione dell'informazione, attraverso i propri algoritmi di ricerca e di selezione dei contenuti
- Instagram promuove una cultura dell'apparenza e della superficialità, attraverso la promozione dell'immagine e del consumo di beni materiali
- Twitter promuove una cultura dell'immediatezza e della superficialità, attraverso la diffusione rapida di messaggi e informazioni senza adeguata verifica

![Battuta privacy](battuta-privacy1.jpg)


## Servizi decentralizzati e liberi

Uno dei principali vantaggi dell'utilizzo di servizi liberi e decentralizzati è la privacy. Con Mastodon, ad esempio, i dati degli utenti non vengono archiviati su un singolo server, ma su diversi server gestiti da diverse comunità.

Inoltre, utilizzando piattaforme decentralizzate, gli utenti hanno maggiore controllo sui propri dati e sulle loro informazioni personali. Invece di concedere l'accesso ai propri dati a un'unica società, gli utenti hanno la possibilità di scegliere il server che ospita i propri dati e di cambiarlo quando lo desiderano.

Un altro vantaggio dell'utilizzo di servizi como Mastodon è la capacità di includere una vasta gamma di comunità e di idee. Invece di essere costretti a seguire le regole di una piattaforma centralizzata e di essere esposti solo a un determinato tipo di contenuto, gli utenti possono scegliere di partecipare a diverse comunità e di esplorare una varietà di contenuti.

![Battuta algoritmi](battuta-algoritmi1.jpg)


## Come fare

Periodicamente ci incontriamo per cercare e provare alternative libere, aiutando chi ha queste esigenze a cambiare programmi, app o servizi che tutti i giorni vengono utilizzati.

Trovi tutti i nostri appuntamenti alla pagina [Eventi](/eventi/).

Indicazioni dove trovarci sulla pagina [Partecipa](/partecipa/).