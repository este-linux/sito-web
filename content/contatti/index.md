---
title: Contatti
slug: contatti
date: 2024-09-22T15:08:00+02:00
include_donations: true
IgnoreTableOfContents: true
images:
  - photo1713598299-3.webp
---

Se ci vuoi contattare per informazioni, per seguirci, aiuti e consigli, un contatto stampa o una mano per la scuola, questi sono tutti i nostri riferimenti.


### 🟢 Email

estelinux(@)mailbox.org


### 🟢 Social

Pagina social nel fediverso:

https://mastodon.uno/@openiteste

### 🟢 Video

Archivio dei video su Peertube:

https://video.linux.it/c/ilseste_channel/videos

### 🟢 Telegram

**ILS Este - Gruppo** (https://t.me/openit_este)

Devi creare un'utenza' su **Telegram** ed entrare nel gruppo.

- [Scarica Telegram dal Play Store](https://play.google.com/store/apps/details?id=org.telegram.messenger)
- [Scarica Telegram da F-Droid](https://f-droid.org/it/packages/org.telegram.messenger/)
- [Scarica Telegram da App Store](https://apps.apple.com/it/app/telegram-messenger/id686449807)


### 🟢 Matrix

**ILS Este - Gruppo** (https://matrix.to/#/!FQaIJQacPkLZHJaMfQ:matrix.org)

E' un canale alternativo e decentralizzato ma sincronizzato con quello di Telegram. Devi creare un'utenza su **Matrix** ed entrare nel gruppo dopo aver scaricato un client per Matrix, noi consigliamo **Element**. Dopodiché dovrai unirti alla stanza da qui: 

- [Scarica Element dal Play Store](https://play.google.com/store/apps/details?id=im.vector.app)
- [Scarica Element da F-Droid](https://f-droid.org/en/packages/im.vector.app/)
- [Scarica Element da App Store](https://apps.apple.com/it/app/element/id1083446067)

