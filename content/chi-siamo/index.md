---
title: Chi siamo
slug: chi-siamo
date: 2022-12-19T22:16:00+02:00
IgnoreTableOfContents: true
include_donations: true
---

**ILS Este** è una sezione locale di ILS a Este, in provincia di Padova, in Veneto. ILS è l'abbreviazione di [Italian Linux Society](https://www.ils.org/), l'associazione italiana di riferimento per la promozione di Linux.

Fondato da [Fabio Lovato](https://loviuz.it) per aggregare persone che vogliono vivere in un mondo digitale libero e rispettoso delle persone, promuovendo software libero a partire da GNU/Linux e contribuendo attivamente.

![Pinguini](antarctic-gce07cbed1_1280.webp)

Discutiamo nel nostro [gruppo Telegram](https://t.me/openit_este) su come la tecnologia ci sta cambiando e come possiamo usarla in modo trasparente e consapevole. In particolare usiamo e promuoviamo software libero come GNU/Linux, app libere e usiamo servizi rispettosi della privacy. Oltre a essere semplici utilizzatori di strumenti digitali aperti, **partecipiamo attivamente** anche per migliorarli poiché è possibile, rispetto a tecnologie chiuse le quali si subiscono solamente.

**GNU/Linux**, chiamato spesso solo Linux, è un sistema operativo (come Windows e Mac per intenderci), che permette di utilizzare il proprio pc senza che sia "una scatola nera che fa cose" a tua insaputa. Tutto il codice di Linux e delle applicazioni che installi sono aperte e libere, ciò significa che le puoi studiare, copiare, modificare e redistribuire in piena libertà, al contrario di ciò che fanno Microsoft e Apple (e molti altri), i quali non ti permettono di vedere dentro.

## [🚀 Scopri come partecipare »](/partecipa/)